<?php 
/* 
 * PayPal and database configuration 
 */ 
  
// PayPal configuration 
// THIS WILL CHANGE TO INFLUENCER'S PAYPAL EMAIL ACCOUNT SO THEY GET THE MONEY SENT TO THEM!
define('PAYPAL_ID', 'abstract_tim-facilitator@mail.com'); //abstract_tim-buyer@mail.com letmein123
define('PAYPAL_SANDBOX', TRUE); //TRUE or FALSE 
 
define('PAYPAL_RETURN_URL', APP_URL.'payment/payment-success'); 
define('PAYPAL_CANCEL_URL', APP_URL.'payment/cancel-payment'); 
define('PAYPAL_NOTIFY_URL', APP_URL.'payment/ipn'); 
define('PAYPAL_CURRENCY', 'GBP'); 

$db_settings = new DBsettings();

// Database configuration 
define('DB_HOST', $db_settings->host); 
define('DB_USERNAME', $db_settings->user); 
define('DB_PASSWORD', $db_settings->pass); 
define('DB_NAME', $db_settings->db); 

//print_r($db_settings);
 
// Change not required 
define('PAYPAL_URL', (PAYPAL_SANDBOX == true)?"https://www.sandbox.paypal.com/cgi-bin/webscr":"https://www.paypal.com/cgi-bin/webscr");