<?php 
require_once TPLATES_PARTS_PATH.'header.php';
//print_r($_POST);
?>

<!-- Page Content -->
<div class="container single-form">

  <div id="row">
    <div id="col text-center">
      <h2>Login</h2>
    </div>
  </div>  

  <div class="row">
    <div class="col-sm"></div>
    <div class="col-sm">
    <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
      <form class="form-signin" name="login_form" id="login_form" action="" method="POST">
        <label for="inputEmail">Email Address</label>
        <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email address" required
          autofocus>
        <label for="inputPassword">Password</label>
        <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password"
          required>
        <!--<div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div> -->
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="login_btn" id="login_btn">Sign in</button>
        <hr />
        <p>Don't have an account? Click <a href="<?=APP_URL?>users/register">here</a> to register.</p>
      </form>
    </div>
    <div class="col-sm"></div>
  </div>

  <!--
  <div class="row login-row">
    <div class="col-sm">
     <h4>Influencer Login</h4>
      <form class="form-signin" name="login_form" id="login_form" action="" method="POST">
        <label for="inputEmail">Email Address</label>
        <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email address" required
          autofocus>
        <label for="inputPassword">Password</label>
        <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password"
          required>
        <!--<div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div> -->
  <!--      <button class="btn btn-lg btn-primary btn-block" type="submit" name="login_btn" id="login_btn">Sign in</button>
        <hr />
        <p>Don't have an account? Click <a href="<?=APP_URL?>users/register">here</a> to register.</p>
      </form>

    </div>

    <div class="col-sm">
    <h4>Brand Login</h4>
      <form class="form-signin_brand" name="login_form" id="login_form" action="" method="POST">
        <label for="inputEmail2">Email Address</label>
        <input type="email" id="inputEmail2" name="inputEmail2" class="form-control" placeholder="Email address" required
          autofocus>
        <label for="inputPassword2">Password</label>
        <input type="password" id="inputPassword2" name="inputPassword2" class="form-control" placeholder="Password"
          required> -->
  <!--<div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div> -->
  <!--     <button class="btn btn-lg btn-primary btn-block" type="submit" name="login_brand_btn" id="login_brand_btn">Sign in</button>
        <hr />
        <p>Don't have an account? Click <a href="<?=APP_URL?>users/register">here</a> to register.</p>
      </form> 

    </div>

  </div> -->

</div>
<!-- /.container -->



<?php require_once TPLATES_PARTS_PATH.'footer.php';?>