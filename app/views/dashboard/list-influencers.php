<?php require_once TPLATES_PARTS_PATH.'header.php';
//print_r($data);
?>

<!-- Page Content -->
<div class="container-fluid">

<?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
  <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?> 

    <div class="row">
        <div class="col-md-2">
            <?php require_once TPLATES_PARTS_PATH.'my-account-menu.php';?>
        </div>
        <div class="col-md-10">
            <!--Table-->
            <table id="table_influencers" class="table table-responsive table-hover table-dark">
                <!--Table head-->
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Mobile</th>
                        <th>Bio</th>
                        <th>Verified</th>
                        <th></th>
                    </tr>
                </thead>
                <!--Table head-->
                <!--Table body-->
                <tbody>
                    <?php foreach ($data['influencers'] as $key => $value) { 
                        $timestamp = strtotime($value['date_added']);
                        $new_date = date("d-m-Y", $timestamp);                    
                    ?>
                    <tr>
                        <th scope="row"><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$value['user_id'];?></a>
                        </th>
                        <td><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$new_date;?></a>
                        </td>                        
                        <td><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$value['firstname'];?></a>
                        </td>
                        <td><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$value['lastname'];?></a>
                        </td>
                        <td><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$value['address'].' ' .$value['postcode'].' ' .$value['country'];?></a>
                        </td>
                        <td><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$value['mobile'];?></a>
                        </td>
                        <td><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$value['bio'];?></a>
                        </td>
                        <td><a href="<?=APP_URL;?>dashboard/edit-influencer/<?=$value['user_id'];?>/"><?=$value['verified'];?></a>
                        </td>
                        <td><button class="btn">Edit</button>
                    </td>
                    </tr>
                    <?php } ?>
                </tbody>
                <!--Table body-->
            </table>
            <!--Table-->
        </div>

    </div>

</div>
<!-- /.container -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>