<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

        <div class="container-fluid">

            <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
            <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

            <div class="row">
            
                <div class="col-md-10">

                    <?php                   
            foreach ($data['requests'] as $key => $value) {
                $timestamp = strtotime($value['date_added']);
                $new_date = date("d-m-Y", $timestamp);
                $keyfound = array_search($value['website_platform_id'], array_column($data['website_platforms'], 'website_platform_id'));                
                ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card text-left">
                                <div class="card-header">
                                    <h5 class="card-title"><?=$value['product_name'];?></h5>
                                </div>
                                <div class="card-body">

                                    <img src="<?=PRODUCT_IMGS_URL.$value['product_image_filename'];?>" 
                                        height="160" border="0" alt="<?=$value['product_name'];?>"
                                        class="product-list-image" />
                                    <p class="card-text">
                                        <?=$value['product_notes'];?>
                                    </p>
                                    <p class="card-text"><a href="<?=$value['product_url'];?>"
                                            target="_blank"><?=substr($value['product_url'], 0, 50);?></a></p>

                                    Date Added: <?=$value['request_date_added'];?><br />
                                    Accepted: <?=$value['request_accepted'];?><br />
                                    <?php
                            $status='info';
                            if($value['request_status']=='Pending')$status='warning';
                            if($value['request_status']=='Complete')$status='success';
                            if($value['request_status']=='Rejected')$status='danger';
                            ?>
                                    Status: <span
                                        class="badge badge-pill badge-<?=$status?>"><?=$value['request_status'];?></span><br />

                                    <?php 
                            if ($value['request_accepted']=='Y') {?>
                                    <p>&nbsp;</p>
                                    <p>You have been accepted to review this product.</p>
                                    <form name="submit_review_btn_form" id="submit_review_btn_form"
                                        action="<?=APP_URL;?>dashboard/add-review" method="POST">
                                        <input type="hidden" name="request_id" id="request_id"
                                            value="<?=$value['review_request_id'];?>">
                                        <input type="hidden" name="product_id" id="product_id"
                                            value="<?=$value['product_id'];?>">
                                        <input type="hidden" name="product_name" id="product_name"
                                            value="<?=$value['product_name'];?>">
                                        <input type="hidden" name="user_id" id="user_id"
                                            value="<?=$value['user_id'];?>">
                                        <input type="hidden" name="influencer_id" id="influencer_id"
                                            value="<?=$value['influencer_id'];?>">
                                        <button name="submit_review_btn" id="submit_review_btn" type="submit"
                                            class="btn btn-primary">Submit Your Review</button>
                                    </form>
                                    <?php
                            }   
                            ?>

                                </div>
                                <div class="card-footer text-muted">
                                    DATE ADDED: <?=$new_date;?> &nbsp; PLATFORM: <a href="#"
                                        class="badge badge-pill badge-info"><?=$data['website_platforms'][$keyfound]['website_platform_icon'];?>
                                        <?=$data['website_platforms'][$keyfound]['website_platform_name'];?></a>
                                    &nbsp; CATEGORY: <a href="#"
                                        class="badge badge-pill badge-info"><?=$value['product_cat_name'];?></a> &nbsp;
                                    REVIEWS: <span
                                        class="badge badge-pill badge-info"><?=$value['num_reviews_submitted'];?></span>
                                    / <span
                                        class="badge badge-pill badge-info"><?=$value['num_reviews_wanted'];?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
            <!-- /.row -->



        </div>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>