<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
      <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>

      <div class="row">

        <?php                   
            foreach ($data['products'] as $key => $value) {
                $timestamp = strtotime($value['date_added']);
                $new_date = date("d-m-Y", $timestamp);                
                ?>
        <div class="row">
          <div class="col-sm">

            <div class="card">
              <div class="card-header">
                <h5><?=$data['products'][0]['product_name'];?></h5>
              </div>
              <div class="card-body">
                <p class="card-text"><strong>Number of reviews needed: </strong>
                  <?=$data['products'][0]['num_reviews_wanted'];?></p>

                <p class="card-text"><strong>Platform: </strong>
                  <?php                                      
              foreach ($data['website_platforms'] as $key => $value) {              
                if ($value['website_platform_id']==$data['products'][0]['website_platform_id']){                
                  echo $value['website_platform_icon']. '<a href="#">' . $value['website_platform_name'] . '</a>';                
                }
              }?>
                </p>
                <p class="card-text"><strong>Category: </strong>
                  <?php            
            foreach ($data['product_categories'] as $key => $value) { 
              if ($value['product_cat_id']==$data['products'][0]['product_cat_id']){                
                echo '<a href="#">' . $value['product_cat_name'] . '</a>';                
              }
            }?>
                </p>

                <p class="card-text"><strong>Product Link: </strong>
                  <a href="<?=$data['products'][0]['product_url'];?>"
                    target="_blank"><?=substr($data['products'][0]['product_url'], 0, 50);?></a>
                </p>

                <p class="card-text"><strong>Notes: </strong>
                  <?=$data['products'][0]['product_notes'];?></p>

                <a href="<?=APP_URL;?>dashboard/apply-to-review/<?=$data['products'][0]['product_id'];?>/"
                  class="btn btn-primary">Apply To Review</a>

              </div>

            </div>

          </div>


          <div class="col-sm">
            <img src="<?=PRODUCT_IMGS_URL.$data['products'][0]['product_image_filename'];?>" width="300" border="0"
              alt="<?=$data['products'][0]['product_name'];?>" class="product-edit-image" />
          </div>

        </div>
        <!-- /.row -->

        <?php } ?>

      </div>
      <!-- /.row -->



    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>