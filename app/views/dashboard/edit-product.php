<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

      <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

      <div class="row">
      
        <div class="col-sm">
          <form name="edit_product_form" id="edit_product_form"
            action="<?=APP_URL;?>dashboard/edit-product/<?=$data['product'][0]['product_id'];?>/" method="POST"
            enctype="multipart/form-data">
            <div class="form-group">
              <label for="product_name">Product</label>
              <input id="product_name" name="product_name" type="text" required="required" class="form-control"
                value="<?=$data['product'][0]['product_name'];?>">
            </div>
            <div class="form-group">
              <label for="num_reviews_wanted">Number of reviews needed</label>
              <input id="num_reviews_wanted" name="num_reviews_wanted" type="number" required="required"
                class="form-control" value="<?=$data['product'][0]['num_reviews_wanted'];?>">
            </div>
            <div class="form-group">
              <label for="website_platform_id">Select platform where the reviews will be made</label>
              <div>
                <select id="website_platform_id" name="website_platform_id" class="custom-select">
                  <?php                                      
              foreach ($data['website_platforms'] as $key => $value) {
                $selected = "";
                if ($value['website_platform_id']==$data['product'][0]['website_platform_id']){$selected = "selected";}
              ?>
                  <option value="<?=$value['website_platform_id'];?>" <?=$selected;?>>
                    <?=$value['website_platform_name'];?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="product_cat_id">Select Product Category</label>
              <div>
                <select id="product_cat_id" name="product_cat_id" class="custom-select">
                  <?php            
            foreach ($data['product_categories'] as $key => $value) { 
              $selected = "";
              if ($value['product_cat_id']==$data['product'][0]['product_cat_id']){$selected = "selected";}
              ?>
                  <option value="<?=$value['product_cat_id'];?>" <?=$selected;?>><?=$value['product_cat_name'];?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="product_url">Product Link</label>
              <input id="product_url" name="product_url" type="text" required="required" class="form-control"
                value="<?=$data['product'][0]['product_url'];?>">
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>*Upload Product Picture:</label>
                <p>Note: This will replace the current product image.</p>
                <input type="file" accept=".png, .jpg, .jpeg" id="product_image" name="product_image"
                  class="form-control" />
                <input type="hidden" name="product_image_filename" id="product_image_filename"
                  value="<?=$data['product'][0]['product_image_filename'];?>">
                <p class="help-block"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="textarea">Notes (optional)</label>
              <textarea id="product_notes" name="product_notes" cols="40" rows="5"
                class="form-control"><?=$data['product'][0]['product_notes'];?></textarea>
            </div>
            <div class="form-group">
              <button name="edit_product_btn" id="edit_product_btn" type="submit" class="btn btn-primary">Update
                Product</button>
            </div>
          </form>
        </div>

        <div class="col-sm">
          <img src="<?=PRODUCT_IMGS_URL.$data['product'][0]['product_image_filename'];?>" width="300" border="0"
            alt="<?=$data['product'][0]['product_name'];?>" class="product-edit-image" />
        </div>

      </div>
      <!-- /.row -->



    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>