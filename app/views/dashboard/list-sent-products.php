<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

        <div class="container-fluid">

            <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
            <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

            <div class="row">

                <div class="col">

                    <!--Table-->
                    <table id="table_products" class="table table-responsive table-hover table-dark">
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Product Link</th>
                                <th>Product Category</th>
                                <th>Influencer</th>
                                <th>Platform</th>
                                <th>Request Status</th>
                                <th>Date Sent</th>
                                <th></th>
                            </tr>
                        </thead>
                        <!--Table head-->
                        <!--Table body-->
                        <tbody>
                            <tr>
                                <td>50</td>
                                <th>This product</th>
                                <td>This link</td>
                                <td>Beauty</td>
                                <td>Tim Davies</td>
                                <td>Amazon</td>
                                <td>Pending</td>
                                <td>01-01-2020</td>
                                <td><a href="<?=APP_URL;?>" class="btn btn-primary">View Request</a></td>
                            </tr>
                        </tbody>
                        <!--Table body-->
                    </table>
                    <!--Table-->

                </div>
            </div>
            <!-- /.row -->



        </div>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>