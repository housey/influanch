<?php

$requests = new Requests();

// Include configuration file for paypal
require_once PAYPAL_PATH.'config.php'; 
 
// Include database connection file for paypal
require_once PAYPAL_PATH.'dbConnect.php'; 
?>
<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

      <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

      <div class="row">

        <div class="col-md-4">
          <div class="card profile-card-2">
            <div class="card-img-block">
              <img class="img-fluid" src="<?=PRODUCT_IMGS_URL.$data['request'][0]['product_image_filename'];?>"
                width="100%" alt="Card image cap" />
            </div>
            <div class="card-body pt-5">
              <img src="<?=PROFILE_IMAGES_URL.$data['request'][0]['profile_image_filename'];?>" alt="profile-image"
                class="profile" />
              <h5 class="card-title card-title-request"><span class="badge badge-secondary">INFLUENCER REQUESTING:
                </span> <span class="badge badge-secondary cat-pill"><a
                    href="<?=APP_URL;?>dashboard/list-influencers/<?=$data['request'][0]['influencer_id'];?>/"><?=$data['request'][0]['firstname'].' '.$data['request'][0]['lastname'];?></a></span>
              </h5>
              <p class="card-text"><strong>Number of reviews made:
                </strong><?=$data['request'][0]['num_reviews_made'];?>
              </p>
              <p class="card-text"><strong>Email: </strong><?=$data['request'][0]['email'];?></p>
              <p class="card-text"><strong>Address:
                </strong><?=$data['request'][0]['address'].' '.$data['request'][0]['postcode'].' '.$data['request'][0]['country'];?>
              </p>
              <p class="card-text"><strong>Bio: </strong><?=$data['request'][0]['bio'];?></p>
              <!-- <p class="card-text"><strong>Categories: </strong><br />
          <?php /*
          $influencer_categories = explode(',',$value['categories']);
          foreach($_SESSION['product_categories'] as $category) {
              if (in_array($category['product_cat_id'], $influencer_categories)) {?>                                
                  <span class="badge badge-secondary cat-pill"><a href="#"><?=$category['product_cat_name'];?></a></span>
              <?php
              }
          }*/
          ?>
        </p>-->

              <p class="card-text"><strong>Website Platforms: </strong><br /></p>

              <div class="icon-block">
                <?php  /*                                    
              foreach ($data['request'][1] as $key => $value) {              
                echo '' <a href="'..'">' . $value['website_platform_name'] . '</a>';                
              }*/?>
                <a href="<?=$data['request'][1]['facebook'];?>" target="_blank" title="facebook"><i
                    class="fab fa-facebook-square"></i></a>
                <a href="<?=$data['request'][1]['linkedin'];?>" target="_blank" title="linkedin"><i
                    class="fab fa-linkedin"></i></a>
                <a href="<?=$data['request'][1]['twitter'];?>" target="_blank" title="twitter"><i
                    class="fab fa-twitter-square"></i></a>
                <a href="<?=$data['request'][1]['instagram'];?>" target="_blank" title="instagram"><i
                    class="fab fa-instagram"></i></a>
                <a href="<?=$data['request'][1]['pinterest'];?>" target="_blank" title="pinterest"><i
                    class="fab fa-pinterest-square"></i></a>
                <a href="<?=$data['request'][1]['youtube'];?>" target="_blank" title="youtube"><i
                    class="fab fa-youtube"></i></a>
                <a href="<?=$data['request'][1]['blog'];?>" target="_blank" title="blog"><i class="fas fa-blog"></i></a>
                <a href="<?=$data['request'][1]['podcast'];?>" target="_blank" title="podcast"><i
                    class="fas fa-podcast"></i></a>
                <a href="<?=$data['request'][1]['amazon'];?>" target="_blank" title="amazon"><i
                    class="fab fa-amazon"></i></a>
                <a href="<?=$data['request'][1]['soundcloud'];?>" target="_blank" title="soundcloud"><i
                    class="fab fa-soundcloud"></i></a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm">
          <?php '<pre>'.print_r($_POST).'</pre>';?>
          <div class="card">
            <div class="card-header">
              <h5><?=$data['request'][0]['product_name'];?></h5>
            </div>
            <div class="card-body">
              <p class="card-text">
                <strong>Date Added: </strong><?=$data['request'][0]['request_date_added'];?><br />
                <strong>Accepted: </strong><?=$data['request'][0]['request_accepted'];?><br />
                <?php
            $status='info';
            //sort this!
            if (isset($_POST['request_status'])) {$req_status = $_POST['request_status'];} else {$req_status = $data['request'][0]['request_status'];}
            if($req_status=='Pending')$status='warning';
            if($req_status=='Complete')$status='success';
            if($req_status=='Rejected')$status='danger';
            ?>
                <strong>Status: </strong><span class="badge badge-pill badge-<?=$status?>"><?=$req_status;?></span>
              </p>

              <p class="card-text"><strong>Number of reviews needed: </strong>
                <?=$data['request'][0]['num_reviews_wanted'];?></p>

              <p class="card-text"><strong>Platform: </strong>
                <?php                                      
              foreach ($data['website_platforms'] as $key => $value) {              
                if ($value['website_platform_id']==$data['request'][0]['website_platform_id']){                
                  echo $value['website_platform_icon'] . ' <a href="#">' . $value['website_platform_name'] . '</a>';                
                }
              }?>
              </p>
              <p class="card-text"><strong>Category: </strong>
                <?php            
            foreach ($data['product_categories'] as $key => $value) { 
              if ($value['product_cat_id']==$data['request'][0]['product_cat_id']){                
                echo '<a href="#">' . $value['product_cat_name'] . '</a>';                
              }
            }?>
              </p>

              <p class="card-text"><strong>Product Link: </strong>
                <a href="<?=$data['request'][0]['product_url'];?>"
                  target="_blank"><?=substr($data['request'][0]['product_url'], 0, 50);?></a>
              </p>

              <p class="card-text"><strong>Notes: </strong>
                <?//=$data['request'][0]['product_notes'];?>
              </p>
              <?php
            if ($data['request'][0]['request_accepted']=='N') { ?>
              <p>When you click accept an email will be sent to the influencer so they can review the product.</p>
              <form name="accept_request_form" id="accept_request_form"
                action="<?=APP_URL;?>dashboard/view-request/<?=$data['request'][0]['review_request_id'];?>/"
                method="POST">
                <input type="hidden" name="review_request_id" id="review_request_id"
                  value="<?=$data['request'][0]['review_request_id'];?>">
                <input type="hidden" name="user_id" id="user_id" value="<?=$data['request'][0]['user_id'];?>">
                <button name="accept_request_btn" id="accept_request_btn" type="submit"
                  class="btn btn-primary">Accept</button>
              </form>
              <?php 
            } else {?>
              <button class="btn btn-primary" disabled>You have accepted this request</button><br /><br />

              <!-- REQUEST STATUS -->
              <hr />
              <h3>Request Status</h3>
              <form name="update_request_form" id="update_request_form"
                action="<?=APP_URL;?>dashboard/view-request/<?=$data['request'][0]['review_request_id'];?>/"
                method="POST">

                <div class="form-group">
                  <label for="request_status">Select Request Status</label>
                  <div>
                    <select id="request_status" name="request_status" class="custom-select">
                      <?php                
                      /***
                       * 
                       * I know what it is now. each status needs to be in an array or object properties
                       * 
                       *    
                       ***/            
                    foreach ($requests->get_options('request_status') as $value) { ?>
                      <option value="<?php $requests->echo_field_data($value,$value);?>"
                        <?php echo (isset($_POST['request_status']) && $value == $_POST['request_status']) ? ' selected="selected"' : ''; ?>>
                        <?php $requests->echo_field_data($value,$value);?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="request_status_notes">Notes (optional)</label>
                  <textarea class="form-control" id="request_status_notes" name="request_status_notes"
                    rows="3"><?php $requests->echo_field_data('request_status_notes',$data['request'][0]['request_status_notes']);?></textarea>
                </div>

                <input type="hidden" name="review_request_id" id="review_request_id"
                  value="<?=$data['request'][0]['review_request_id'];?>">
                <button name="update_request_status_btn" id="update_request_status_btn" type="submit"
                  class="btn btn-primary">Update Request Status</button>
              </form>

              <?php } ?>

              <!-- PRODUCT STATUS -->
              <hr />
              <h3>Product Status</h3>
              <form name="update_product_form" id="update_product_form"
                action="<?=APP_URL;?>dashboard/view-request/<?=$data['request'][0]['review_request_id'];?>/"
                method="POST">

                <div class="form-group">
                  <label for="product_status">Select Product Status</label>
                  <div>
                    <select id="product_status" name="product_status" class="custom-select">
                      <?php           
                    foreach ($requests->get_options('product_status') as $value) { ?>
                      <option value="<?php $requests->echo_field_data($value,$value);?>"
                        <?php echo (isset($_POST['product_status']) && $value == $_POST['product_status']) ? ' selected="selected"' : ''; ?>>
                        <?php $requests->echo_field_data($value,$value);?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="product_sent_date">Product Sent Date</label>
                  <div>
                    <input class="form-control" type="date"
                      value="<?php $requests->echo_field_data('product_sent_date',date("Y-m-d"));?>"
                      id="product_sent_date" name="product_sent_date">
                  </div>
                </div>

                <div class="form-group">
                  <label for="product_status_notes">Notes (optional)</label>
                  <textarea class="form-control" id="product_status_notes" name="product_status_notes"
                    rows="3"><?php $requests->echo_field_data('product_status_notes',$data['request'][0]['product_status_notes']);?></textarea>
                </div>

                <input type="hidden" name="review_request_id" id="review_request_id"
                  value="<?=$data['request'][0]['review_request_id'];?>">
                <button name="update_product_status_btn" id="update_product_status_btn" type="submit"
                  class="btn btn-primary">Update Product Status</button>
              </form>

              <!-- PAYMENT STATUS -->
              <hr />
              <h3>Payment Status</h3>
              <form name="update_payment_form" id="update_payment_form"
                action="<?=APP_URL;?>dashboard/view-request/<?=$data['request'][0]['review_request_id'];?>/"
                method="POST">

                <div class="form-group">
                  <label for="payment_status">Select Payment Status</label>
                  <div>
                    <select id="payment_status" name="payment_status" class="custom-select">
                      <?php             
                    foreach ($requests->get_options('payment_status') as $value) { ?>
                      <option value="<?php $requests->echo_field_data($value,$value);?>"
                        <?php echo (isset($_POST['payment_status']) && $value == $_POST['payment_status']) ? ' selected="selected"' : ''; ?>>
                        <?php $requests->echo_field_data($value,$value);?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="payment_date">Payment Date</label>
                  <div>
                    <input class="form-control" type="date"
                      value="<?php $requests->echo_field_data('payment_sent_date',date("Y-m-d"));?>"
                      id="payment_sent_date" name="payment_sent_date">
                  </div>
                </div>

                <div class="form-group">
                  <label for="payment_status_notes">Notes (optional)</label>
                  <textarea class="form-control" id="payment_status_notes" name="payment_status_notes"
                    rows="3"><?php $requests->echo_field_data('payment_status_notes',$data['request'][0]['payment_status_notes']);?></textarea>
                </div>

                <input type="hidden" name="review_request_id" id="review_request_id"
                  value="<?=$data['request'][0]['review_request_id'];?>">
                <button name="update_payment_status_btn" id="update_payment_status_btn" type="submit"
                  class="btn btn-primary">Update Payment Status</button>

              </form>

              <!-- MAKE PAYMENT -->
              <hr />
              <h3>Make Payment</h3>

              <!-- PayPal payment form for displaying the buy button -->
              <form action="<?php echo PAYPAL_URL; ?>" method="post">
                <!-- Identify your business so that you can collect the payments. -->
                <input type="hidden" name="business" value="<?php echo PAYPAL_ID; ?>">

                <!-- Specify a Buy Now button. -->
                <input type="hidden" name="cmd" value="_xclick">

                <!-- Specify details about the item that buyers will purchase. -->
                <input type="hidden" name="item_name" value="Influencer Product">
                <input type="hidden" name="item_number" value="106">
                <label for="amount">Amount to pay</label>
                <input type="text" class="form-control" name="amount" value="1.00">
                <input type="hidden" name="currency_code" value="<?php echo PAYPAL_CURRENCY; ?>">

                <!-- Specify URLs -->
                <input type="hidden" name="return" value="<?php echo PAYPAL_RETURN_URL; ?>">
                <input type="hidden" name="cancel_return" value="<?php echo PAYPAL_CANCEL_URL; ?>">
                <input type="hidden" name="notify_url" value="<?php echo PAYPAL_NOTIFY_URL; ?>">

                <!-- Display the payment button. -->
                <p>&nbsp;</p>

                <button name="make_payment_btn" id="make_payment_btn" type="submit" class="btn btn-primary pp_btn"><i
                    class="fab fa-paypal pp_icon"></i> SEND PAYMENT</button>

              </form>

              <hr />
              <small><strong>FOR TESTING PAYPAL PAYMENT USE THIS:<br /> abstract_tim-buyer@mail.com letmein123</strong><br />When you make a PayPal payment the payment status will automatically change to 'Payment Sent' and
                the amount and date will be shown.</small>

            </div>

          </div>

        </div>

        <div class="col-sm">
          <img src="<?=PRODUCT_IMGS_URL.$data['request'][0]['product_image_filename'];?>" width="300" border="0"
            alt="<?=$data['request'][0]['product_name'];?>" class="product-edit-image" />
        </div>

      </div>
      <!-- /.row -->


    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>