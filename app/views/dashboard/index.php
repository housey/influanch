<?php require_once TPLATES_PARTS_PATH.'header.php';?>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <div class="col-md-8 mb-5">
        <h2><?=$data['page_title'];?></h2>
        <hr>
        <?php require_once TPLATES_PARTS_PATH.'dashboard-menu.php';?>
      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <?php require_once TPLATES_PARTS_PATH.'footer.php';?>