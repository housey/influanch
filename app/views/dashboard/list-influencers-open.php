<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

    <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
    <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

    <div class="row">
    
        <div class="col-md-10">

            <div class="row">
                <?php                   
            foreach ($data['influencers'] as $key => $value) {
                $timestamp = strtotime($value['date_added']);
                $new_date = date("d-m-Y", $timestamp);                
                ?>
                <div class="col-md-4">
                    <div class="card profile-card-2">
                        <div class="card-body pt-5">
                            <img src="<?=PROFILE_IMAGES_URL.$value['profile_image_filename'];?>"
                                alt="profile-image center" class="profile-2" />
                            <h5 class="card-title card-title-request"><span class="badge badge-secondary">INFLUENCER:
                                </span> <span
                                    class="badge badge-secondary"><?=$value['firstname'].' '.$value['lastname'];?></span>
                            </h5>
                            <p class="card-text"><strong>Number of reviews made:
                                </strong><?=$value['num_reviews_made'];?>
                            </p>
                            <p class="card-text"><strong>Email: </strong><?=$value['email'];?></p>
                            <p class="card-text"><strong>Address:
                                </strong><?=$value['address'].' '.$value['postcode'].' '.$value['country'];?>
                            </p>
                            <p class="card-text"><strong>Bio: </strong><?=$value['bio'];?></p>
                            <p class="card-text"><strong>Categories: </strong><br />
                                <?php 
                            $influencer_categories = explode(',',$value['categories']);
                            foreach($_SESSION['product_categories'] as $category) {
                                if (in_array($category['product_cat_id'], $influencer_categories)) {?>
                                <span class="badge badge-secondary cat-pill"><a
                                        href="<?=APP_URL;?>dashboard/view-products/<?=$category['product_cat_id'].'/'.$category['product_cat_slug'];?>"><?=$category['product_cat_name'];?></a></span>
                                <?php
                                }
                            }
                            ?>
                            </p>

                            <p class="card-text"><strong>Website Platforms: </strong><br /></p>

                            <div class="icon-block">

                                <a href="<?=$value['facebook'];?>" target="_blank" title="facebook"><i
                                        class="fab fa-facebook-square"></i></a>
                                <a href="<?=$value['linkedin'];?>" target="_blank" title="linkedin"><i
                                        class="fab fa-linkedin"></i></a>
                                <a href="<?=$value['twitter'];?>" target="_blank" title="twitter"><i
                                        class="fab fa-twitter-square"></i></a>
                                <a href="<?=$value['instagram'];?>" target="_blank" title="instagram"><i
                                        class="fab fa-instagram"></i></a>
                                <a href="<?=$value['pinterest'];?>" target="_blank" title="pinterest"><i
                                        class="fab fa-pinterest-square"></i></a>
                                <a href="<?=$value['youtube'];?>" target="_blank" title="youtube"><i
                                        class="fab fa-youtube"></i></a>
                                <a href="<?=$value['blog'];?>" target="_blank" title="blog"><i
                                        class="fas fa-blog"></i></a>
                                <a href="<?=$value['podcast'];?>" target="_blank" title="podcast"><i
                                        class="fas fa-podcast"></i></a>
                                <a href="<?=$value['amazon'];?>" target="_blank" title="amazon"><i
                                        class="fab fa-amazon"></i></a>
                                <a href="<?=$value['soundcloud'];?>" target="_blank" title="soundcloud"><i
                                        class="fab fa-soundcloud"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <?php } ?>

            </div><!-- end row -->

        </div>

    </div>



    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>