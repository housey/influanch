<?php require_once TPLATES_PARTS_PATH.'header.php';
//print_r($data);
?>

<!-- Page Content -->
<div class="container-fluid">

  <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
  <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>  

  <div class="row">
    <div class="col-md-2">
      <?php require_once TPLATES_PARTS_PATH.'my-account-menu.php';?>
    </div>
    <div class="col-sm">
      <form name="edit_review_form" id="edit_review_form"
        action="<?=APP_URL;?>dashboard/view-review/<?=$data['review'][0]['review_id'];?>/" method="POST">
        <div class="form-group">
          <label for="website_platform_id">Select platform where review was made</label>
          <div>
            <select id="website_platform_id" name="website_platform_id" class="custom-select">
              <?php                            
              foreach ($data['website_platforms'] as $key => $value) {
                $selected = "";
                echo $value['website_platform_id']." ".$data['website_platforms'][0]['website_platform_id']."<br/>";
                if ($value['website_platform_id']==$data['review'][0]['website_platform_id']){$selected = "selected";}
              ?>
              <option value="<?=$value['website_platform_id'];?>" <?=$selected;?>><?=$value['website_platform_name'];?>
              </option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="review_url">Review Link</label>
          <input id="review_url" name="review_url" value="<?=$data['review'][0]['review_url']?>" type="text"
            required="required" class="form-control">
        </div>
        <div class="form-group">
          <label for="textarea">Notes (optional)</label>
          <textarea id="notes" name="notes" cols="40" rows="5"
            class="form-control"><?=$data['review'][0]['notes']?></textarea>
        </div>
        <div class="form-group">
          <button name="edit_review_btn" id="edit_review_btn" type="submit" class="btn btn-primary">Update
            Review</button>
        </div>
      </form>
    </div>

    <div class="col-sm">
    </div>

  </div>
  <!-- /.row -->

</div>
<!-- /.container -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>