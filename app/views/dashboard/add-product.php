<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

      <div class="row">
        <div class="col">
          <!--<h1 class="mt-4">My Account</h1>-->
          <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
          <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
        </div>
      </div>

      <div class="row">

        <div class="col-sm">
          <form name="add_product_form" id="add_product_form" action="<?=APP_URL;?>dashboard/add-product" method="POST"
            enctype="multipart/form-data">
            <div class="form-group">
              <label for="product_name">Product</label>
              <input id="product_name" name="product_name" type="text" required="required" class="form-control">
            </div>
            <div class="form-group">
              <label for="num_reviews_wanted">Number of reviews needed</label>
              <input id="num_reviews_wanted" name="num_reviews_wanted" type="number" required="required"
                class="form-control">
            </div>
            <div class="form-group">
              <label for="website_platform_id">Select platform where the reviews will be made</label>
              <div>
                <select id="website_platform_id" name="website_platform_id" class="custom-select">
                  <?php                            
              foreach ($data['website_platforms'] as $key => $value) {?>
                  <option value="<?=$value['website_platform_id'];?>"><?=$value['website_platform_name'];?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="product_cat_id">Select Product Category</label>
              <div>
                <select id="product_cat_id" name="product_cat_id" class="custom-select">
                  <?php foreach ($data['product_categories'] as $key => $value) { ?>
                  <option value="<?=$value['product_cat_id'];?>"><?=$value['product_cat_name'];?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="product_url">Product Link</label>
              <input id="product_url" name="product_url" type="text" required="required" class="form-control">
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>*Upload Product Picture:</label>
                <input type="file" accept=".png, .jpg, .jpeg" id="product_image" name="product_image"
                  class="form-control" required />
                <p class="help-block"></p>
              </div>
            </div>
            <div class="form-group">
              <label for="textarea">Notes (optional)</label>
              <textarea id="product_notes" name="product_notes" cols="40" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <button name="add_product_btn" id="add_product_btn" type="submit" class="btn btn-primary">Add
                Product</button>
            </div>
          </form>
        </div>

        <div class="col-sm">
        </div>

      </div>
      <!-- /.row -->



    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>