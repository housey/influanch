<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

        <div class="container-fluid">

            <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
            <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

            <div class="row">

                <div class="col">

                    <!--Table-->
                    <table id="table_products" class="table table-responsive table-hover table-dark">
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Image</th>
                                <th>Product Link</th>
                                <th>Product Category</th>
                                <th>Influencer</th>
                                <th>Platform</th>
                                <th>Request Status</th>
                                <th>Product Status</th>
                                <th>Payment Status</th>
                                <th>Date Added</th>
                                <th></th>
                            </tr>
                        </thead>
                        <!--Table head-->
                        <!--Table body-->
                        <tbody>
                            <?php foreach ($data['requests'] as $key => $value) { ?>
                            <?php
                        $timestamp = strtotime($value['date_added']);
                        $new_date = date("d-m-Y", $timestamp);
                        $keyfound = array_search($value['website_platform_id'], array_column($data['website_platforms'], 'website_platform_id')); 
                        $status='info';
                        if($value['request_status']=='Pending')$status='warning';
                        if($value['request_status']=='Complete')$status='success';
                        if($value['request_status']=='Rejected')$status='danger';
                        ?>
                            <tr>
                                <th scope="row"><a
                                        href="<?=APP_URL;?>dashboard/view-request/<?=$value['review_request_id'];?>/"><?=$value['review_request_id'];?></a>
                                </th>
                                <td><a
                                        href="<?=APP_URL;?>dashboard/view-request/<?=$value['review_request_id'];?>/"><?=$value['product_name'];?></a>
                                </td>
                                <td><img src="<?=PRODUCT_IMGS_URL.$value['product_image_filename'];?>" width="128" />
                                </td>
                                <td><a href="<?=$value['product_url'];?>"
                                        target="_blank"><?=substr($value['product_url'], 0, 50);?></a></td>
                                <td><a
                                        href="<?=APP_URL;?>dashboard/view-products/<?=$value['product_cat_id'].'/'.$value['product_cat_slug'];?>"><?=$value['product_cat_name'];?></a>
                                </td>
                                <td><a href="<?=APP_URL;?>dashboard/list-influencers/<?=$value['influencer_id'];?>/"><img
                                            src="<?=PROFILE_IMAGES_URL.$value['profile_image_filename'];?>"
                                            width="16" /> <?=$value['firstname'].' '.$value['lastname'];?></a></td>
                                <td><?=$data['website_platforms'][$keyfound]['website_platform_icon'];?>
                                    <?=$data['website_platforms'][$keyfound]['website_platform_name'];?></td>
                                <td><span
                                        class="badge badge-pill badge-<?=$status?>"><?=$value['request_status'];?></span>
                                </td>
                                <td><?=$value['product_status'];?></td>
                                <td><?=$value['payment_status'];?></td>
                                <td><?=$value['request_date_added'];?></td>
                                <td><a href="<?=APP_URL;?>dashboard/view-request/<?=$value['review_request_id'];?>/"
                                        class="btn btn-primary">View Request</a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <!--Table body-->
                    </table>
                    <!--Table-->

                </div>
            </div>
            <!-- /.row -->



        </div>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>