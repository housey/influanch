<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

        <div class="container-fluid">

            <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
            <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

            <div class="row">

                <div class="col">
                    <!--Table-->
                    <table id="table_reviews" class="table table-responsive table-hover table-dark">
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Product</th>
                                <th>Image</th>
                                <th>Product Link</th>
                                <th>Influencer</th>
                                <th>Platform</th>
                                <th>Review Link</th>
                                <th>Review Notes</th>
                                <th></th>
                            </tr>
                        </thead>
                        <!--Table head-->
                        <!--Table body-->
                        <tbody>
                            <?php foreach ($data['reviews'] as $key => $value) { 
                        $timestamp = strtotime($value['date_added']);
                        $new_date = date("d-m-Y", $timestamp);                    
                    ?>
                            <tr>
                                <th scope="row"><a
                                        href="<?=APP_URL;?>dashboard/view-review/<?=$value['review_id'];?>/"><?=$value['review_id'];?></a>
                                </th>
                                <td><a
                                        href="<?=APP_URL;?>dashboard/view-review/<?=$value['review_id'];?>/"><?=$new_date;?></a>
                                </td>
                                <td><a href="<?=APP_URL;?>dashboard/view-review/<?=$value['review_id'];?>/">
                                        <?=$value['product_name'];?></a>
                                </td>
                                <td>
                                <img src="<?=PRODUCT_IMGS_URL.$value['product_image_filename'];?>" width="128" />
                                </td>
                                <td><a href="<?=$value['product_url'];?>"
                                        target="_blank"><?=substr($value['product_url'], 0, 50);?></a>

                                </td>
                                <td><a href="<?=APP_URL;?>dashboard/list-influencers/<?=$value['influencer_id'];?>/"><img
                                            src="<?=PROFILE_IMAGES_URL.$value['profile_image_filename'];?>"
                                            width="16" /> <?=$value['firstname'].' '.$value['lastname'];?></a>
                                </td>



                                <td><a href="<?=APP_URL;?>dashboard/view-review/<?=$value['review_id'];?>/"><?=$value['wplat']['website_platform_icon'];?>
                                        <?=$value['wplat']['website_platform_name'];?></a>
                                </td>
                                <td><a href="<?=$value['review_url'];?>"
                                        target="_blank"><?=substr($value['review_url'], 0, 50);?></a></td>
                                <td><a
                                        href="<?=APP_URL;?>dashboard/view-review/<?=$value['review_id'];?>/"><?=$value['notes'];?></a>
                                </td>
                                <td>
                                <td><a href="<?=APP_URL;?>dashboard/view-review/<?=$value['review_id'];?>/"
                                        class="btn btn-primary">View Review</a></td>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <!--Table body-->
                    </table>
                    <!--Table-->
                </div>

            </div>



        </div>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>