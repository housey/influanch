<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

      <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

      <div class="row">
      
        <div class="col-sm">
          <h1><span class="badge badge-pill badge-info">Add Review For</span> <?=$data['request']['product_name'];?>
          </h1>

          <form name="add_review_form" id="add_review_form" action="<?=APP_URL;?>dashboard/add-review" method="POST">
            <input type="hidden" name="request_id" id="request_id" value="<?=$data['request']['review_request_id'];?>">
            <input type="hidden" name="product_id" id="product_id" value="<?=$data['request']['product_id'];?>">
            <input type="hidden" name="product_name" id="product_name" value="<?=$data['request']['product_name'];?>">
            <input type="hidden" name="user_id" id="user_id" value="<?=$data['request']['user_id'];?>">
            <input type="hidden" name="influencer_id" id="influencer_id"
              value="<?=$data['request']['influencer_id'];?>">
            <div class="form-group">
              <label for="website_platform_id">Select platform where review was made</label>
              <div>
                <select id="website_platform_id" name="website_platform_id" class="custom-select">
                  <?php foreach ($data['website_platforms'] as $key => $value) { ?>
                  <option value="<?=$value['website_platform_id'];?>"><?=$value['website_platform_name'];?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="review_url">Review Link</label>
              <input id="review_url" name="review_url" placeholder="" type="url" required="required"
                class="form-control">
            </div>
            <div class="form-group">
              <label for="textarea">Notes (optional)</label>
              <textarea id="notes" name="notes" cols="40" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <button name="add_review_btn" id="add_review_btn" type="submit" class="btn btn-primary">Add
                Review</button>
            </div>
          </form>

        </div>

        <div class="col-sm">
        </div>

      </div>



    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>