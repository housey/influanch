<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

        <div class="container-fluid">

            <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
            <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

            <div class="row">
            
                <div class="col">

                    <!--Table-->
                    <table id="table_products" class="table table-responsive table-hover table-dark">
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>#</th>                                
                                <th>Product</th>
                                <th>Image</th>
                                <th>Product Link</th>
                                <th>Product Category</th>
                                <th>Platform</th>
                                <th>Num Reviews Wanted</th>
                                <th>Num Reviews Submitted</th>
                                <th></th>
                            </tr>
                        </thead>
                        <!--Table head-->
                        <!--Table body-->
                        <tbody>
                            <?php foreach ($data['products'] as $key => $value) { 
                            $keyfound = array_search($value['website_platform_id'], array_column($data['website_platforms'], 'website_platform_id'));?>
                            <tr>
                                <th scope="row"><a
                                        href="<?=APP_URL;?>dashboard/edit-product/<?=$value['product_id'];?>/"><?=$value['product_id'];?></a>
                                </th>                                
                                <td><a href="<?=APP_URL;?>dashboard/edit-product/<?=$value['product_id'];?>/"><?=$value['product_name'];?></a>
                                </td>
                                <td>
                                <img src="<?=PRODUCT_IMGS_URL.$value['product_image_filename'];?>" width="128" />
                                </td>
                                <td><a href="<?=$value['product_url'];?>"
                                        target="_blank"><?=substr($value['product_url'], 0, 50);?></a></td>
                                <td><a
                                        href="<?=APP_URL;?>dashboard/view-products/<?=$value['pcat']['product_cat_id'].'/'.$value['pcat']['product_cat_slug'];?>"><?=$value['pcat']['product_cat_name'];?></a>
                                </td>
                                <td><?=$data['website_platforms'][$keyfound]['website_platform_icon'];?>
                                    <?=$data['website_platforms'][$keyfound]['website_platform_name'];?></td>
                                <td><span class="badge badge-pill badge-info"><?=$value['num_reviews_wanted'];?></span>
                                </td>
                                <td><span
                                        class="badge badge-pill badge-info"><?=$value['num_reviews_submitted'];?></span>
                                </td>
                                <td><a href="<?=APP_URL;?>dashboard/edit-product/<?=$value['product_id'];?>/"
                                        class="btn btn-primary">Edit Product</a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <!--Table body-->
                    </table>
                    <!--Table-->

                </div>
            </div>
            <!-- /.row -->



        </div>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>