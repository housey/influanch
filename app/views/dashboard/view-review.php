<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

      <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

      <div class="row">
      
        <div class="col-sm">

          <div class="card">
            <div class="card-header">
              <h5><?=$data['product'][0]['product_name'];?></h5>
            </div>
            <div class="card-body">

              <p class="card-text"><strong>Platform: </strong>
                <?php                                      
              foreach ($data['website_platforms'] as $key => $value) {              
                if ($value['website_platform_id']==$data['product'][0]['website_platform_id']){                
                  echo $value['website_platform_icon']. ' <a href="#">' . $value['website_platform_name'] . '</a>';                
                }
              }?>
              </p>
              <p class="card-text"><strong>Category: </strong>
                <?php            
            foreach ($data['product_categories'] as $key => $value) { 
              if ($value['product_cat_id']==$data['product'][0]['product_cat_id']){?>
                <a
                  href="<?=APP_URL;?>dashboard/view-products/<?=$value['product_cat_id'].'/'.$value['product_cat_slug'];?>"><?=$value['product_cat_name'];?></a>
                <?php } 
            }?>
              </p>

              <p class="card-text"><strong>Product Link: </strong>
                <a href="<?=$data['product'][0]['product_url'];?>"
                  target="_blank"><?=substr($data['product'][0]['product_url'], 0, 50);?></a>
              </p>

              <p class="card-text"><strong>Notes: </strong>
                <?=$data['product'][0]['product_notes'];?></p>

              <hr />

              <h5>Reviewed by Influencer:
                <?=$data['influencer'][0]['firstname'].' '.$data['influencer'][0]['lastname'];?></h5>

              <p class="card-text"><strong>Review Added: </strong>
                <?=$data['review'][0]['date_added'];?></p>

              <p class="card-text"><strong>Review Link: </strong>
                <a href="<?=$data['review'][0]['review_url'];?>"
                  target="_blank"><?=substr($data['review'][0]['review_url'], 0, 50);?></a></p>

              <p class="card-text"><strong>Review Notes: </strong>
                <?=$data['review'][0]['notes'];?></p>

              <hr />

              <a href="<?=APP_URL;?>dashboard/list-reviews/" class="btn btn-primary">Back to reviews</a>

              <a href="#" class="btn btn-primary pp_btn">Show Review Request</a>

            </div>

          </div>


        </div>

        <div class="col-sm">
          <img src="<?=PRODUCT_IMGS_URL.$data['product'][0]['product_image_filename'];?>" width="300" border="0"
            alt="<?=$data['product'][0]['product_name'];?>" class="product-edit-image" />
        </div>

      </div>
      <!-- /.row -->



    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>