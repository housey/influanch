<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

  <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

    <div class="container-fluid">

      <div class="row">
        <div class="col">
          <!--<h1 class="mt-4">My Account</h1>-->
          <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
          <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
        </div>
      </div>


      <div class="row">

        <div class="col-sm">
          <form name="edit_user_form" id="edit_user_form" action="<?=APP_URL;?>profile" method="POST" autocomplete="off"
            enctype="multipart/form-data">
            <h3>Category Settings</h3>
            <label>*Categories</label>
            <div class="controls">
              <button type="button" name="checkAll" id="checkAll" class="btn btn-primary">Select/Unselect All
              </button>
              <hr />
              <p>&nbsp;</p>
              <?php          
            foreach($_SESSION['product_categories'] as $category) { 
              if (in_array($category['product_cat_id'], explode(',',$_SESSION['USER']['categories']))) {
                $checked = 'checked';
              } else {
                $checked = '';
              }?>
              <label class="label-category">
                <input type="checkbox" name="categories[]" value="<?=$category['product_cat_id'];?>"
                  id="<?=$category['product_cat_id'];?>"
                  <?= $checked; ?>><?=$category['product_cat_name'];?></label><br />
              <?php
            }
          ?>
              <p class="help-block"></p>
            </div>
        </div>


        <div class="col-sm">
          <h3>Profile Settings</h3>
          <?php 
        if ($data['user_data'][0]['profile_image_filename']!='') {?>
          <img src="<?=PROFILE_IMAGES_URL.$data['user_data'][0]['profile_image_filename'];?>" class="profile-pic center"
            border="0" alt="profile pic" />
          <?php } ?>
          <div class="form-group">
            <label for="firstname">*Firstname</label>
            <input id="firstname" name="firstname" type="text" class="form-control"
              value="<?=$data['user_data'][0]['firstname'];?>" required>
          </div>
          <div class="form-group">
            <label for="lastname">*Lastname</label>
            <input id="lastname" name="lastname" type="text" class="form-control"
              value="<?=$data['user_data'][0]['lastname'];?>" required>
          </div>
          <div class="form-group">
            <label for="text">*Email</label>
            <input id="email" name="email" placeholder="email" type="email" class="form-control"
              value="<?=$data['user_data'][0]['email'];?>" disabled>
          </div>
          <div class="form-group">
            <label for="password">*Password</label>
            <input id="password" name="password" type="password" class="form-control"
              value="<?=$data['user_data'][0]['password'];?>" autocomplete="new-password" required>
          </div>
          <div class="form-group">
            <label for="confirm-password">*Confirm Password</label>
            <input id="confirm-password" name="confirm-password" type="password" class="form-control"
              value="<?=$data['user_data'][0]['password'];?>" autocomplete="new-password" required>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Upload Profile Picture:</label>
              <p>Note: This will replace the current profile image.</p>
              <input type="file" accept=".png, .jpg, .jpeg" id="profile_image" name="profile_image"
                class="form-control" />
              <input type="hidden" name="profile_image_filename" id="profile_image_filename"
                value="<?=$data['user_data'][0]['profile_image_filename'];?>">
              <p class="help-block"></p>
            </div>
          </div>

        </div>
        <div class="col-sm">
          <h3>&nbsp;</h3>
          <div class="form-group">
            <label for="textarea">*Bio</label>
            <textarea id="bio" name="bio" cols="40" rows="5" class="form-control"
              required><?=$data['user_data'][0]['bio'];?></textarea>
          </div>
          <div class="form-group">
            <label for="textarea">*Address</label>
            <textarea id="address" name="address" cols="40" rows="5" class="form-control"
              required><?=$data['user_data'][0]['address'];?></textarea>
          </div>
          <div class="form-group">
            <label for="postcode">*Postcode</label>
            <input id="postcode" name="postcode" type="text" class="form-control"
              value="<?=$data['user_data'][0]['postcode'];?>" required>
          </div>
          <div class="form-group">
            <label for="text">*Country</label>
            <input id="country" name="country" placeholder="England" type="text" class="form-control"
              value="<?=$data['user_data'][0]['country'];?>" required>
          </div>
          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input id="mobile" name="mobile" type="text" class="form-control"
              value="<?=$data['user_data'][0]['mobile'];?>" maxlength="20">
          </div>
          <div class="form-group">
            <label for="paypal_email"><i class="fab fa-paypal"></i> PayPal Email Address</label>
            <input id="paypal_email" name="paypal_email" type="email" class="form-control"
              value="<?=$data['user_data'][0]['paypal_email'];?>">
          </div>

        </div>
        <div class="col-sm">
          <h3>Platform Settings</h3>
          <div class="form-group">
            <label for="facebook"><i class="fab fa-facebook-square"></i> Facebook</label>
            <input id="facebook" name="facebook" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['facebook']) ? $data['user_data'][0]['facebook']: '';?>">
          </div>
          <div class="form-group">
            <label for="linkedin"><i class="fab fa-linkedin"></i> Linkedin</label>
            <input id="linkedin" name="linkedin" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['linkedin']) ? $data['user_data'][0]['linkedin']: '';?>">
          </div>
          <div class="form-group">
            <label for="twitter"><i class="fab fa-twitter-square"></i> Twitter</label>
            <input id="twitter" name="twitter" placeholder="" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['twitter']) ? $data['user_data'][0]['twitter']: '';?>">
          </div>
          <div class="form-group">
            <label for="instagram"><i class="fab fa-instagram"></i> Instagram</label>
            <input id="instagram" name="instagram" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['instagram']) ? $data['user_data'][0]['instagram']: '';?>">
          </div>
          <div class="form-group">
            <label for="pinterest"><i class="fab fa-pinterest-square"></i> Pinterest</label>
            <input id="pinterest" name="pinterest" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['pinterest']) ? $data['user_data'][0]['pinterest']: '';?>">
          </div>

        </div>
        <div class="col-sm">
          <h3>&nbsp;</h3>
          <div class="form-group">
            <label for="youtube"><i class="fab fa-youtube"></i> Youtube</label>
            <input id="youtube" name="youtube" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['youtube']) ? $data['user_data'][0]['youtube']: '';?>">
          </div>
          <div class="form-group">
            <label for="blog"><i class="fas fa-blog"></i> Blog</label>
            <input id="blog" name="blog" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['blog']) ? $data['user_data'][0]['blog']: '';?>">
          </div>
          <div class="form-group">
            <label for="podcast"><i class="fas fa-podcast"></i> Podcast</label>
            <input id="podcast" name="podcast" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['podcast']) ? $data['user_data'][0]['podcast']: '';?>">
          </div>
          <div class="form-group">
            <label for="amazon"><i class="fab fa-amazon"></i> Amazon</label>
            <input id="amazon" name="amazon" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['amazon']) ? $data['user_data'][0]['amazon']: '';?>">
          </div>
          <div class="form-group">
            <label for="soundcloud"><i class="fab fa-soundcloud"></i> Soundcloud</label>
            <input id="soundcloud" name="soundcloud" type="url" class="form-control"
              value="<?php echo isset($data['user_data'][0]['soundcloud']) ? $data['user_data'][0]['soundcloud']: '';?>">
          </div>
          <p>&nbsp;</p>
          <p>Fields marked with * are required.</p>
          <p>&nbsp;</p>
          <div class="form-group">
            <button name="edit_user_btn" type="submit" class="btn btn-lg btn-primary btn-block">Update Profile</button>
          </div>

        </div>





      </div>
      <!--end row -->

      </form>




    </div>

  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>