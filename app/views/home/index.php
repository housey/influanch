<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<?php
$user_logged_in = false;
if (isset($_SESSION['USER']['loggedin']) && $_SESSION['USER']['loggedin']==1){
  $user_logged_in = true;
}
if (isset($data['msg'])) {
?>
<div class="container">
  <div class="row">
    <div class="col">
      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
    </div>
  </div>
</div>
<?php } ?>
<header class="home-header">
  <div class="container">
    <div class="jumbotron">
      <h1 class="display-4">InfluLaunch</h1>
      <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt neque tempore recusandae
        animi
        soluta quasi? Asperiores rem dolore eaque vel, porro, soluta unde debitis aliquam laboriosam. Repellat
        explicabo,
        maiores!</p>
      <hr class="my-4">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt neque tempore recusandae animi soluta
        quasi?
        Asperiores rem dolore eaque vel, porro, soluta unde debitis aliquam laboriosam.</p>
      <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
    </div>
  </div>
</header>
<!-- Page Content -->

<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <h2>What We Do</h2>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt neque tempore recusandae animi soluta
          quasi? Asperiores rem dolore eaque vel, porro, soluta unde debitis aliquam laboriosam. Repellat explicabo,
          maiores!</p>
        <p>Donec suscipit sapien ut augue pretium, non tincidunt nisl porttitor. Quisque ornare id mi id hendrerit.
          Suspendisse sem nisl, sagittis vel tortor at, placerat mollis nisl. Vivamus nec odio tristique, commodo metus
          aliquet, viverra tortor. Nullam vitae vestibulum mauris. Phasellus dui enim, vulputate et varius ac, porta vel
          est. Integer varius lectus non odio laoreet, vel accumsan metus molestie. Curabitur eget urna urna. Curabitur
          quis euismod dolor.</p>
        <p><a class="btn btn-primary btn-lg" href="<?=APP_URL;?>users/register/">Sign up &raquo;</a></p>
      </div>
      <div class="col-sm-6">

        <h2>How It Works</h2>
        <hr>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt neque tempore recusandae animi soluta
          quasi? Asperiores rem dolore eaque vel, porro, soluta unde debitis aliquam laboriosam. Repellat explicabo,
          maiores!</p>

        <p><a class="btn btn-primary btn-lg" href="#">Read more &raquo;</a></p>

      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

</div>
<!-- /.container -->

<section class="home-bottom">
  <div class="container">
    <div class="jumbotron">
      <h1 class="display-4">InfluLaunch</h1>
      <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt neque tempore recusandae
        animi
        soluta quasi? Asperiores rem dolore eaque vel, porro, soluta unde debitis aliquam laboriosam. Repellat
        explicabo,
        maiores!</p>
      <hr class="my-4">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt neque tempore recusandae animi soluta
        quasi?
        Asperiores rem dolore eaque vel, porro, soluta unde debitis aliquam laboriosam.</p>
      <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
    </div>
  </div>
</section>

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>