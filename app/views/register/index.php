<?php require_once TPLATES_PARTS_PATH.'header.php';
//print_r($_SESSION);
?>

<!-- Page Content -->
<div class="container single-form">
  <div id="row">
    <div id="col text-center">
      <h2>Register</h2>
    </div>
  </div>

  <div class="row">
    <div class="col-sm"></div>
    <div class="col-sm">
      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
      <form class="form-signup" name="signup_form" id="signup_form" action="" method="POST">

        <label for="firstname">*First Name</label>
        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="Joe" maxlength="20"
          required autofocus>

        <label for="lastname">*Last Name</label>
        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Bloggs" maxlength="20"
          required>

        <label for="email">*Email Address</label>
        <input type="email" id="email" name="email" class="form-control" placeholder="Email address" maxlength="200"
          required>
        <label for="password">*Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="new-password" required>

        <label for="confirm-password">*Confirm Password</label>
        <input type="password" id="confirm-password" name="confirm-password" class="form-control"
          placeholder="Confirm Password" autocomplete="new-password" required>

        <label><strong>*Your Favourite Categories:</strong></label>
        <p>Please select the categories that you are interested in receiving and reviewing products from.</p>
        <div class="controls">
          <button type="button" name="checkAll" id="checkAll" class="btn btn-primary" >Select/Unselect All
          </button>
          <br />
          <?php 
            foreach($_SESSION['product_categories'] as $category) { ?>
          <label class="label-category">
            <input type="checkbox" name="categories[]" value="<?=$category['product_cat_id'];?>"
              id="<?=$category['product_cat_id'];?>">
            <?=$category['product_cat_name'];?></label><br />
          <?php
            }
          ?>
          <p class="help-block"></p>
        </div>       
        <p>Fields marked with * are required.</p>      
        <hr />
        <div class="controls">
          <input type="checkbox" name="terms" value="terms" id="terms" required
            data-validation-required-message="You must agree to the terms and conditions.">
          I agree with our <a href="#" target="_blank" title="Terms and Conditions">Terms &amp;
            Conditions</a>&nbsp;<span class="fa fa-external-link"></span>.
          </label>
        </div>
        <p>&nbsp;</p>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="signup_btn" id="signup_btn">Sign
          Up</button>
        <hr />
        <p>Already have an account? <a href="<?=APP_URL?>users/login">Click here</a>.</p>

      </form>
    </div>
    <div class="col-sm"></div>
  </div>
  <!-- /.row -->

</div>
<!-- /.container -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>