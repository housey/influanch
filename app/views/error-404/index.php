<?php header("HTTP/1.0 404 Not Found"); ?>
<?php require_once TPLATES_PARTS_PATH.'header.php'; ?>

<!-- Page Content -->
<div class="container single-form" id="error-404">

  <div class="row">
    <div class="col-sm"></div>
    <div class="col-sm">
      <h2 class="text-center">Error 404</h2>
      <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
      <h4>Oops! We're sorry the page you're looking for could not be found!</h4>
      <p>&nbsp;</p>
      <div class="exclamation"><p><i class="fas fa-exclamation-circle"></i></p></div>
      <p>&nbsp;</p>
      <p class="text-center">It may have been have moved or no longer exists.</p>
      <p class="text-center">Please use the menu to try and find what you are looking for or get in <a
          href="<?=APP_URL;?>contact-us">contact</a>.</p>
    </div>
    <div class="col-sm"></div>
    
  </div>
  <hr />
</div>
<!-- /.container -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>