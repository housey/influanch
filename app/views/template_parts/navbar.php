<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <!-- fixed-top -->
  <div class="container">
    <a class="navbar-brand" href="<?=APP_URL;?>"><i class="fas fa-rocket"></i> InfluLaunch</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsDefault"
      aria-controls="navbarsDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?=APP_URL;?>"><i class="fas fa-home"></i> Home <span
              class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fas fa-question-circle"></i> About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fas fa-info-circle"></i> Support</a>
        </li>
        <?php
      if ($user_logged_in) {
        if ($admin_logged_in) {?>
        <li class="nav-item">
          <a class="nav-link admin-link" href="<?=APP_URL;?>my-account"><i class="fas fa-lock"></i> Admin</a>
        </li>
        <?php } else { ?>
        <li class="nav-item">
          <a class="nav-link user-link" href="<?=APP_URL;?>my-account"><i class="fas fa-rocket"></i> My Account</a>
        </li>
        <?php 
        }
      }
      if (!$user_logged_in) {?>
        <li class="nav-item">
          <a class="nav-link" href="<?=APP_URL;?>users/login"><i class="fas fa-sign-in-alt"></i> Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=APP_URL;?>users/register"><i class="fas fa-user-plus"></i> Register</a>
        </li>
        <?php } ?>
        <?php
      if ($user_logged_in) {?>
        <li class="nav-item">
          <a class="nav-link" href="<?=APP_URL;?>users/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
        </li>
        <?php } ?>
        <li class="nav-item">
          <a class="nav-link" href="<?=APP_URL;?>contact-us"><i class="fas fa-envelope-open-text"></i> Contact Us</a>
        </li>
      </ul>
    </div>
  </div>
</nav>