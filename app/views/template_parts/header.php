<?php 
//move this!
$user_logged_in = false;
$admin_logged_in = false;
if (isset($_SESSION['USER']['loggedin']) && $_SESSION['USER']['loggedin']==1){
  $user_logged_in = true;
  if (isset($_SESSION['USER']['role']) && $_SESSION['USER']['role']=='admin'){
    $admin_logged_in = true;
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>InfluLaunch</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="https://kit.fontawesome.com/abd4872814.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,500" rel="stylesheet">
    <link href="<?=APP_URL;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=APP_URL;?>css/style.css" rel="stylesheet">  
</head>
<body>

<?php require_once 'navbar.php';?>


