<?php 
if (isset($data['msg']) && isset($data['msg-class'])) { ?>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <div class="alert alert-<?=$data['msg-class'];?>" role="alert">
                <?=$data['msg'];?>
            </div>
        </div>
    </div>
</div>
<?php } ?>