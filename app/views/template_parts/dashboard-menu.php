
<h4>Products</h4>
<hr />
<a href="<?=APP_URL;?>dashboard/list-products" class="btn btn-secondary btn-lg btn-block">List/Edit Products</a>
<a href="<?=APP_URL;?>dashboard/add-product" class="btn btn-secondary btn-lg btn-block">Add Product</a>
<hr />
<h4>Reviews</h4>
<hr />
<a href="<?=APP_URL;?>dashboard/list-reviews" class="btn btn-secondary btn-lg btn-block">List/Edit Reviews</a>    
<a href="<?=APP_URL;?>dashboard/add-review" class="btn btn-secondary btn-lg btn-block">Add Review</a>        