<!-- Footer -->
<footer class="py-5 footer">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; InfluLaunch <?=date('Y');?></p>
  </div>
  <!-- /.container -->
</footer>
<script src="<?=APP_URL;?>vendor/jquery/jquery.min.js"></script>
<script src="<?=APP_URL;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
  $(document).ready(function () {
    $("#checkAll").click(function () {
      var checkBoxes = $("input[name=categories\\[\\]]");
      checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });    

    $("#dash-nav a").each(function () {
      var path = window.location.protocol + "//" + window.location.host + location.pathname;
  
  /*  path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);*/
      var href = $(this).attr('href');
        
      if (path.substring(0, href.length) === href) {
        $(this).addClass('active2');
      }
    });
  });

  $("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    $("#toggle_icon").toggleClass("fas fa-toggle-on fas fa-toggle-off");
    
  });
  
</script>

</body>

</html>