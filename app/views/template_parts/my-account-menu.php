<?php

if ($admin_logged_in) {
/*** Admin Menu ***/
?>
<ul class="list-group hover-none my-account-menu">
    <li class="list-group-item list-group-item-dark"><a class="dropdown-item" href="<?=APP_URL;?>my-account"><i class="fas fa-lock"></i> Admin Account</a></li>
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>profile"><i class="fas fa-user-edit"></i> Profile Settings</a></li>    
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/add-product"><i class="fas fa-pen"></i> Add Product</a></li>
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-influencers"><i class="fas fa-user"></i> List Influencers</a></li>    
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-requests"><i class="fas fa-clipboard-list"></i> List Requests
    <?php if ($_SESSION['USER']['num_pending_requests']>0) {?><span class="badge badge-info"><?=$_SESSION['USER']['num_pending_requests'];?></span><span class="sr-only">unaccepted requests</span><?php }?></a></li>
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-products"><i class="fas fa-clipboard-list"></i> List Products</a></li>
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-reviews"><i class="fas fa-clipboard-list"></i> List Reviews</a></li>
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-sent-products"><i class="fas fa-box"></i> Products Sent</a></li>
    <li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-payments"><i class="fas fa-receipt"></i> Payments</a></li>
    </li>
</ul>
<?php } else {
/*** User Menu ***/    
?>
<ul class="list-group hover-none my-account-menu">
<li class="list-group-item list-group-item-dark"><a class="nav-link" href="<?=APP_URL;?>my-account"><i class="fas fa-rocket"></i> Influencer Account</a></li>
<li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>profile"><i class="fas fa-user-edit"></i> Profile Settings</a></li>
<li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-products"><i class="fas fa-clipboard-list"></i> List Products</a></li>
<li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-requests"><i class="fas fa-clipboard-list"></i> List Requests
<?php if ($_SESSION['USER']['num_accepted_requests']>0) {?><span class="badge badge-info"><?=$_SESSION['USER']['num_accepted_requests'];?></span><span class="sr-only">unaccepted requests</span><?php }?></a></li>
<li class="list-group-item"><a class="dropdown-item" href="<?=APP_URL;?>dashboard/list-reviews"><i class="fas fa-clipboard-list"></i> Review History</a></li>
<li class="list-group-item"><a class="dropdown-item" href="#"><i class="fas fa-award"></i> Rewards</a></li>
</li>
</ul>
<?php } ?>