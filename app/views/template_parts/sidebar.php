<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading">
      <?php if ($admin_logged_in) { ?>
      ADMINISTRATOR
      <?php } else { ?>
      INFLUENCER
      <?php } ?>
    </div>
    <div class="list-group list-group-flush" id="dash-nav">
      <?php require_once TPLATES_PARTS_PATH.'my-account-menu-2.php';?>
    </div>
  </div>
  <!-- /#sidebar-wrapper -->