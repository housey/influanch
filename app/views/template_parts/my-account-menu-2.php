<?php
if ($admin_logged_in) {
/*** Admin Menu ***/
?>
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>my-account"><i class="fas fa-lock"></i> Admin Account</a>
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>profile"><i class="fas fa-user-edit"></i> Profile Settings</a>    
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/add-product"><i class="fas fa-pen"></i> Add Product</a>
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-influencers"><i class="fas fa-user"></i> List Influencers</a>    
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-requests"><i class="fas fa-clipboard-list"></i> List Requests
    <?php if ($_SESSION['USER']['num_pending_requests']>0) {?><span class="badge badge-info"><?=$_SESSION['USER']['num_pending_requests'];?></span><span class="sr-only">unaccepted requests</span><?php }?></a>
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-products"><i class="fas fa-clipboard-list"></i> List Products</a>
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-reviews"><i class="fas fa-clipboard-list"></i> List Reviews</a>
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-sent-products"><i class="fas fa-box"></i> Products Sent</a>
    <a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-payments"><i class="fas fa-receipt"></i> Payments</a>
<?php } else {
/*** User Menu ***/    
?>
<a class="nav-link" href="<?=APP_URL;?>my-account"><i class="fas fa-rocket"></i> Influencer Account</a>
<a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>profile"><i class="fas fa-user-edit"></i> Profile Settings</a>
<a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-products"><i class="fas fa-clipboard-list"></i> List Products</a>
<a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-requests"><i class="fas fa-clipboard-list"></i> List Requests
<?php if ($_SESSION['USER']['num_accepted_requests']>0) {?><span class="badge badge-info"><?=$_SESSION['USER']['num_accepted_requests'];?></span><span class="sr-only">unaccepted requests</span><?php }?></a>
<a class="list-group-item list-group-item-action bg-light" href="<?=APP_URL;?>dashboard/list-reviews"><i class="fas fa-clipboard-list"></i> Review History</a>
<a class="list-group-item list-group-item-action bg-light" href="#"><i class="fas fa-award"></i> Rewards</a>
<?php } ?>