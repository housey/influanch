<?php require_once TPLATES_PARTS_PATH.'header.php';?>

<!-- Page Content -->
<div class="container">

<?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
  <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?> 

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header bg-brand-color text-white"><i class="fa fa-envelope"></i> Contact us</div>
                <div class="card-body">
                <form name="contact_form" id="contact_form" action="<?=APP_URL;?>contact-us" method="POST">
                        <div class="form-group">
                            <label for="name">*Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="email">*Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="<?php if (isset($_SESSION['USER']['email'])) echo $_SESSION['USER']['email'];?>" required>
                        </div>
                        <div class="form-group">
                            <label for="message">*Message</label>
                            <textarea class="form-control" id="message" name="message" rows="6" required></textarea>
                        </div>
                        <div class="mx-auto">
                        <p>Fields marked with an * are required.</p>
                        <button type="submit" class="btn btn-primary text-right" name="contact_form_btn" id="contact_form_btn">Submit</button></div>
                    </form>
                </div>
            </div>
        </div>        
        <div class="col-12 col-sm-4">
        <?php /* ?>
            <div class="card bg-light mb-3">
                <div class="card-header bg-brand-color text-white text-uppercase"><i class="fa fa-home"></i> Address</div>
                <div class="card-body">
                    <p>3 rue des Champs Elysées</p>
                    <p>75008 PARIS</p>
                    <p>France</p>
                    <p>Email : email@example.com</p>
                    <p>Tel. +33 12 56 11 51 84</p>

                </div>

            </div>
            <?php */ ?>
        </div> 
    </div>


</div>
<!-- /.container -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>