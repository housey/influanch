<?php require_once TPLATES_PARTS_PATH.'header.php';?>

<!-- Page Content -->
<div class="container-fluid">

  <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
  <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>

  <div class="row">
    <div class="col-md-2">
      <?php require_once TPLATES_PARTS_PATH.'my-account-menu.php';?>
    </div>
    <div class="col-sm">
      <div class="card bg-light mb-3">
        <div class="card-header">Profile</div>
        <div class="card-body card-profile">
     
            <?php 
        if ($data['user_data'][0]['profile_image_filename']!='') {?>
            <img src="<?=PROFILE_IMAGES_URL.$data['user_data'][0]['profile_image_filename'];?>" class="profile-pic center"
              border="0" alt="profile pic" width="160" />
            <?php } ?>
            <h5 class="card-title">Personal Information</h5>
       
          <table border="0" cellpadding="10" class="table table-dark">
            <tbody>
              <tr>
                <td><strong>First name:</strong></td>
                <td><?=$data['user_data'][0]['firstname'];?></td>
              </tr>
              <tr>
                <td><strong>Last name:</strong></td>
                <td><?=$data['user_data'][0]['lastname'];?></td>
              </tr>
              <tr>
                <td><strong>Email:</strong></td>
                <td><?=$data['user_data'][0]['email'];?></td>
              </tr>
              <tr>
                <td><strong>Address:</strong></td>
                <td><?=$data['user_data'][0]['address'];?></td>
              </tr>
              <tr>
                <td><strong>Postcode:</strong></td>
                <td><?=$data['user_data'][0]['postcode'];?></td>
              </tr>
              <tr>
                <td><strong>Country:</strong></td>
                <td><?=$data['user_data'][0]['country'];?></td>
              </tr>
              <tr>
                <td><strong>Mobile:</strong></td>
                <td><?=$data['user_data'][0]['mobile'];?></td>
              </tr>
              <tr>
                <td><strong>Bio:</strong></td>
                <td><?=$data['user_data'][0]['bio'];?></td>
              </tr>
            </tbody>
          </table>

          <a class="btn btn-primary btn-lg btn-right" href="<?=APP_URL;?>profile" role="button">Edit
            Profile</a>
        </div>
      </div>
    </div>

    <div class="col-sm">
    </div>

  </div>
</div>
<!-- /.container -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>