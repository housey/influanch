<?php require_once TPLATES_PARTS_PATH.'header.php';?>
<div class="d-flex" id="wrapper">

    <?php require_once TPLATES_PARTS_PATH.'sidebar.php';?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php require_once TPLATES_PARTS_PATH.'sidebar-navbar.php';?>

        <div class="container-fluid">

            <div class="row">
                <div class="col">
                    <!--<h1 class="mt-4">My Account</h1>-->
                    <?php require_once TPLATES_PARTS_PATH.'page-breadcrumbs.php';?>
                    <?php require_once TPLATES_PARTS_PATH.'msg-alerts.php';?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-sm">

                    <div class="status">
                        <?php if(!empty($payment_id)){ ?>
                        <h1 class="success">Your Payment has been Successful</h1>

                        <h4>Payment Information</h4>
                        <p><b>Reference Number:</b> <?php echo $payment_id; ?></p>
                        <p><b>Transaction ID:</b> <?php echo $txn_id; ?></p>
                        <p><b>Paid Amount:</b> <?php echo $payment_gross; ?></p>
                        <p><b>Payment Status:</b> <?php echo $payment_status; ?></p>

                        <h4>Product Information</h4>
                        <p><b>Name:</b> <?php echo $product_row['name']; ?></p>
                        <p><b>Price:</b> <?php echo $product_row['price']; ?></p>
                        <?php }else{ ?>
                        <h1 class="error">Your Payment has Failed</h1>
                        <?php } ?>
                    </div>
                    <a href="<?=APP_URL;?>dashboard" class="btn-link">Back to Dashboard</a>

                </div>

                <div class="col-sm">
                </div>

            </div>



        </div>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php require_once TPLATES_PARTS_PATH.'footer.php';?>