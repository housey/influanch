<?php
class Influencers extends User
{
    public function __construct() {
        $this->db = new DB;
    }

    public function influencer_exists($influencer_id) {
        $sql = "SELECT * FROM ".TABLE_USER." WHERE user_id=".$influencer_id." AND role = 'influencer' LIMIT 1";
        $result = $this->db->query_2_array($sql);
        return count($result);
    }

    public function get_influencers() {
        $sql = "SELECT * FROM ".TABLE_USER." 
        INNER JOIN ".TABLE_USER_WEBSITE_PLATFORMS." ON ".TABLE_USER.".user_id = ".TABLE_USER_WEBSITE_PLATFORMS.".user_id
        WHERE ".TABLE_USER.".role = 'influencer'";
        $result = $this->db->query_2_array($sql);
        return $result;
    }

    public function get_influencer_by_id($influencer_id) {
        $sql = "SELECT * FROM ".TABLE_USER." 
        INNER JOIN ".TABLE_USER_WEBSITE_PLATFORMS." ON ".TABLE_USER.".user_id = ".TABLE_USER_WEBSITE_PLATFORMS.".user_id
        WHERE ".TABLE_USER.".user_id = ".$influencer_id." LIMIT 1";
        $result = $this->db->query_2_array($sql);
        return $result;
    }
}
?>