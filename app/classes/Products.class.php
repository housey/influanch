<?php

class Products 
{

    public $db;
    private $product_slug;

    public function __construct () {
        $this->db = new DB();
    }

    public function product_exists($product_id) {
        $sql = "SELECT * FROM ".TABLE_PRODUCTS." WHERE product_id=".$product_id." LIMIT 1";
        $result = $this->db->query_2_array($sql);
        return count($result);
    }

    public function get_products () {
        $sql = "SELECT * FROM ".TABLE_PRODUCTS." ORDER BY product_id DESC";
        $result = $this->db->query_2_array($sql);
        foreach ($result as $key => $value) {
            $result[$key]['pcat'] = $this->get_product_category_by_id($value['product_cat_id']);
        }
        return $result;
    }

    public function get_product_by_id ($product_id) {
        $sql = "SELECT * FROM ".TABLE_PRODUCTS." WHERE product_id = ".$product_id." LIMIT 1";
        $result = $this->db->query_2_array($sql);
        $result['pcat'] = $this->get_product_category_by_id($product_id);
        return $result;
    }

    public function get_products_by_cat_id ($category_id) {
        $sql = "SELECT * FROM ".TABLE_PRODUCTS." WHERE product_cat_id = ".$category_id;
        $result = $this->db->query_2_array($sql);
        foreach ($result as $key => $value) {
            $result[$key]['pcat'] = $this->get_product_category_by_id($value['product_cat_id']);
        }
        return $result;
    }

    public function get_products_by_user ($user_id) {
        $sql = "SELECT * FROM ".TABLE_PRODUCTS." WHERE user_id = ".$user_id;
        $result = $this->db->query_2_array($sql);        
        foreach ($result as $key => $value) {
            $result[$key]['pcat'] = $this->get_product_category_by_id($value['product_cat_id']);
        }
        return $result;
    }

    Public function get_product_category_by_id ($product_cat_id) {
        $sql = "SELECT * FROM ".TABLE_PRODUCT_CATEGORIES." WHERE product_cat_id = $product_cat_id LIMIT 1";
        $result = $this->db->query_2_array($sql);
        return @$result[0];
    }

    public function add_product ($user_id, $product_name, $product_url, $product_notes, $num_reviews_wanted, $product_cat_id, $website_platform_id,$product_image_filename) {
        $date_added = date('Y-m-d');
        $system = new System();
        $this->product_slug = $system->create_slug($product_name);    
        $sql = "INSERT INTO ".TABLE_PRODUCTS." (user_id, product_name, product_url, product_notes, num_reviews_wanted, product_cat_id, website_platform_id,product_image_filename,product_slug, date_added) VALUES ('$user_id', '$product_name', '$product_url', '$product_notes', '$num_reviews_wanted', '$product_cat_id', '$website_platform_id', '$product_image_filename','$this->product_slug', '$date_added')";
        $this->db->query($sql);
        return $this->db->get_insert_id();
    }

    public function edit_product ($user_id, $product_id, $product_name, $product_url, $product_notes, $num_reviews_wanted, $product_cat_id,$website_platform_id,$product_image_filename) {     
        $sql = "UPDATE ".TABLE_PRODUCTS." SET user_id='$user_id', product_cat_id='$product_cat_id', product_name='$product_name', product_url='$product_url', product_notes='$product_notes', num_reviews_wanted='$num_reviews_wanted', website_platform_id='$website_platform_id', product_image_filename='$product_image_filename' WHERE  product_id=".$product_id;
        return $this->db->query($sql);
    }      

    Public function get_product_categories () {
        $sql = "SELECT * FROM ".TABLE_PRODUCT_CATEGORIES;
        $result = $this->db->query_2_array($sql);
        return $result;
    }

    public function incremenet_review_count($product_id) {
        $sql = "UPDATE ".TABLE_PRODUCTS." SET num_reviews_submitted=num_reviews_submitted+1 WHERE product_id=".$product_id;
        return $this->db->query($sql);
    }

}