<?php

class Reviews 
{

    public $db;

    public function __construct () {
        $this->db = new DB();
    }

    public function review_exists($review_id) {
        $sql = "SELECT * FROM ".TABLE_REVIEWS." WHERE review_id = ".$review_id." LIMIT 1";
        $result = $this->db->query_2_array($sql);
        return count($result);
    }

    public function get_review_by_id ($review_id) {
        $sql = "SELECT * FROM ".TABLE_REVIEWS." WHERE review_id = ".$review_id;
        $result = $this->db->query_2_array($sql);
        return $result;
    }

    Public function get_website_platforms() {
        $sql = "SELECT * FROM ".TABLE_WEBSITE_PLATFORMS;
        $result = $this->db->query_2_array($sql);
        return $result;
    }

    Public function get_website_platform_by_id($website_platform_id) {
        $sql = "SELECT * FROM ".TABLE_WEBSITE_PLATFORMS." WHERE website_platform_id = $website_platform_id LIMIT 1";
        $result = $this->db->query_2_array($sql);
        return @$result[0];
    }

    public function add_review($user_id, $influencer_id, $product_id, $website_platform_id, $review_url, $notes) {      
        $date_added = date('Y-m-d');
        $sql = "INSERT INTO ".TABLE_REVIEWS." (user_id, influencer_id, product_id, website_platform_id, review_url, notes, date_added) VALUES ('$user_id', '$influencer_id', '$product_id', '$website_platform_id', '$review_url', '$notes','$date_added')";

        $mailer = new Sysmail();

        //email BRAND
        $mailer->send_email('', $user_id, 
        "Influlaunch: Someone has submitted a review for your product", 
        "Someone has submitted to review for your product, please login. <p>Kind Regards,</p> <p> Influlaunch</p>");

        //email INFLUENCER        
        $mailer->send_email($_SESSION['USER']['firstname'], $influencer_id, 
        "Influlaunch: Your product review has been submitted", 
        "Your product review has been submitted and sent to the product owner, Thank you.<p>Kind Regards,</p> <p> Influlaunch</p>");

        return $this->db->query($sql);
    }    

    public function edit_review($user_id,$website_platform_id,$review_url,$review_id,$notes) {
        //$date_added = date('Y-m-d');                
        $sql = "UPDATE ".TABLE_REVIEWS." SET user_id='$user_id', website_platform_id='$website_platform_id', review_url='$review_url', notes='$notes' WHERE  review_id=".$review_id;        
        return $this->db->query($sql);
    }

    public function get_reviews_by_user($user_id) {

        $sql = "
        SELECT * FROM ".TABLE_REVIEWS." 
        INNER JOIN ".TABLE_PRODUCTS." ON ".TABLE_REVIEWS.".product_id = ".TABLE_PRODUCTS.".product_id
        INNER JOIN ".TABLE_PRODUCT_CATEGORIES." ON ".TABLE_PRODUCT_CATEGORIES.".product_cat_id = ".TABLE_PRODUCTS.".product_cat_id 
        INNER JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id = ".TABLE_REVIEWS.".influencer_id        
        WHERE ".TABLE_REVIEWS.".user_id = ".$user_id;

        $result = $this->db->query_2_array($sql);
        foreach ($result as $key => $value) {
            $result[$key]['wplat'] = $this->get_website_platform_by_id($value['website_platform_id']);
        }
        return $result;
    }

    public function get_reviews_by_influencer($influencer_id) {

        $sql = "
        SELECT * FROM ".TABLE_REVIEWS." 
        INNER JOIN ".TABLE_PRODUCTS." ON ".TABLE_REVIEWS.".product_id = ".TABLE_PRODUCTS.".product_id
        INNER JOIN ".TABLE_PRODUCT_CATEGORIES." ON ".TABLE_PRODUCT_CATEGORIES.".product_cat_id = ".TABLE_PRODUCTS.".product_cat_id        
        INNER JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id = ".TABLE_REVIEWS.".influencer_id
        WHERE influencer_id = ".$influencer_id;

        $result = $this->db->query_2_array($sql);
        foreach ($result as $key => $value) {
            $result[$key]['wplat'] = $this->get_website_platform_by_id($value['website_platform_id']);
        }
        return $result;
    }

}