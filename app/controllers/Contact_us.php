<?php

class Contact_Us extends Controller
{
    public function index() {
        $this->contactus = $this->model('Contactusmodel');
        if (isset($_POST['contact_form_btn']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message'])) {
           if ($this->contactus->send_contact_email($_POST)) {
            $this->view('contact-us/index', ['page_title'=>'Contact Us', 'msg'=>'You message has been sent. we will be in touch shortly.','msg-class'=>'success']);
            exit;
           } else {
            $this->view('contact-us/index', ['page_title'=>'Contact Us', 'msg'=>'There was a problem sending your message please try again later.','msg-class'=>'danger']);
            exit;
           }
        }
        $this->view('contact-us/index', ['page_title'=>'Contact Us']); 
    }
}