<?php

class Users extends Controller
{

    public $login, $logout, $product_data, $register, $myaccount, $pending_requests, $user_data;   

    public function __construct() {        
        $this->login = $this->model('Login');
        $this->logout = $this->model('Login');      
        $this->product_data = $this->model('Dashboardmodel');
        $this->register = $this->model('register');  
        $this->myaccount = $this->model('myaccountmodel');
        $this->requests = $this->model('Requests');    
        $this->num_pending_requests = $_SESSION['USER']['num_pending_requests'];
        $this->num_accepted_requests = $_SESSION['USER']['num_accepted_requests'];  
    }

    public function index() {
        $this->refuse_non_authed();       
        $this->view('home/index');
    }    

    public function login() {    
        if (isset($_POST['inputEmail']) && isset($_POST['inputEmail']) && isset($_POST['inputPassword'])) {        

            $this->login->login_user($_POST['inputEmail'],$_POST['inputPassword']);
            if (isset($_SESSION['USER']) && isset($_SESSION['USER']['loggedin'])){

                $verified = $this->login->check_user_verified($_SESSION['USER']['userid']);
                if ($verified === false) {
                    $this->logout->logout();
                    $this->view('login/index', ['page_title'=>'Login','msg'=>'Login error: Your account is not verified, please check your email.', 'msg-class'=>'danger']);
                    exit;
                }  

                //SORT all this out - needs to be different for Admin/User etc.
                $_SESSION['USER']['num_pending_requests'] = $this->requests->get_num_pending_requests($_SESSION['USER']['userid']);
                $_SESSION['USER']['num_accepted_requests'] = $this->requests->get_num_accepted_requests($_SESSION['USER']['userid']);                            

                $login_msg = 'Welcome '.$_SESSION['USER']['firstname'].'.';

                if ($this->num_pending_requests > 0) $login_msg .= " You have {$this->num_pending_requests} review requests pending.";
                if ($this->num_accepted_requests > 0) $login_msg .= " You have {$this->num_accepted_requests} accepted requests pending.";
                
                $_SESSION['product_categories'] = $this->product_data->get_product_categories();
                header('Location: '.APP_URL.'my-account');                  
            } else {
                $this->view('login/index', ['page_title'=>'Login','msg'=>'Login error: Either your email or password was incorrect.', 'msg-class'=>'danger']);
            }
        }   
        if (isset($_SESSION['USER']) && isset($_SESSION['USER']['loggedin']) && $_SESSION['USER']['loggedin']==1){
            $this->view('my-account/index', ['page_title'=>'My Account']);
        } else {
            $this->view('login/index', ['page_title'=>'Login']);
        }         
    }

    public function logout() {    
        $this->logout->logout();
        header('Location: '.APP_URL);
        exit;
    }    

    public function register() {
        
        $_SESSION['product_categories'] = $this->product_data->get_product_categories();
        
        if (isset($_SESSION['USER']) && isset($_SESSION['USER']['loggedin']) && $_SESSION['USER']['loggedin']==1){
            header('Location: '.APP_URL.'my-account'); 
            exit;
        }    
                          
        if (isset ($_SESSION['USER']['loggedin']) && $_SESSION['USER']['loggedin']==1 && isset($_POST['signup_btn'])) {
            header('Location: '.BASE_URL);   
            exit;              
        }        

        if (isset($_POST['signup_btn']) && isset($_POST['email']) && isset($_POST['password'])) {

            /* check terms box was checked */
            if (!isset($_POST['terms'])) {
                $this->view('register/index', ['page_title'=>'Register', 'msg'=>'You must agree to our terms and conditions.', 'msg-class'=>'danger']);                   
                exit;
            }

            /* check if user already exists */
            if ($this->register->user_exists($_POST['email'])) {
                $this->view('register/index', ['page_title'=>'Register', 'msg'=>'An account with this email address already exists.', 'msg-class'=>'danger']);                   
                exit;
            }

            /* check passwords match */
            if ($_POST['confirm-password'] != $_POST['password']) {
                $this->view('register/index', ['page_title'=>'Register', 'msg'=>'Your passwords do not match.', 'msg-class'=>'danger']);
                exit;
            }

            /* check that a category is checked */
            if (!isset($_POST['categories'])) {
                $this->view('register/index', ['page_title'=>'Register', 'msg'=>'You must select at least one category', 'msg-class'=>'danger']);
                exit;
            }

            /* sign up user */
            if ($this->register->signup_user()) {
                $this->view('register/index', ['page_title'=>'Register','msg'=>'We have sent you a verification email to complete sign up. Please check your email (as well as your spam folder).','msg-class'=>'info']);        
                exit;
            } else {
                $this->view('register/index', ['page_title'=>'Register','msg'=>'There was a problem with your registration.','msg-class'=>'danger']);        
                exit;
            }      

        } else {
            $this->view('register/index', ['page_title'=>'Register']);
        }               
    }

    public function verify_user($token,$user_id) {

        $verified = $this->login->check_user_verified($user_id);
        if ($verified === true) {    
            $this->view('home/index', ['page_title'=>'Login','msg'=>'You have successfully verified your account, you may now login <a href="'.BASE_URL.'users/login/">here</a>.','msg-class'=>'success']);
            exit;
        }      
        
        $token = trim($token);
        $user_id = trim($user_id);

        $result = $this->register->verify_user($token,$user_id);       
        if ($result === true) {
            $this->register->send_welcome_mail($user_id);
            $this->view('home/index', ['page_title'=>'Login','msg'=>'You have successfully verified your account, you may now login <a href="'.BASE_URL.'users/login/">here</a>.','msg-class'=>'success']);
        } else {
            $this->view('login/index', ['page_title'=>'Login','msg'=>'There was a problem verifiying your account. You have already verified you may login below.','msg-class'=>'danger']);
        }
    }
    

}