<?php

class Dashboard extends Controller
{

    private $dashboard;
    private $requests;

    public function __construct() {
        
        $this->refuse_non_authed();
        $this->dash = $this->model('Dashboardmodel');
        $this->requests = $this->model('Requests');
        $this->website_platforms = $this->dash->get_website_platforms();    
        $this->product_categories = $this->dash->get_product_categories();
        $this->products = $this->dash->get_products();
    }

    public function index() {
        $this->view('home/index', ['page_title'=>'Home']);
    }
    
    /***
     * 
     * PRODUCTS
     * 
     ***/

    public function list_products($view='table', $category='all') {      
        
        if (isset($_POST['apply_request_btn']) && isset($_POST['product_id']) && isset($_POST['product_name']) && isset($_POST['user_id'])) {
            if ($this->requests->check_user_request($_SESSION['USER']['userid'], $_POST['product_id'])) {
                $this->view('dashboard/list-products-open', ['page_title'=>'Dashboard - Product List', 'products'=>$this->products, 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms,'msg'=>'You have already requested to review this product.', 'msg-class'=>'danger']);
                exit;
            }
            $this->requests->add_request($_POST['user_id'], $_SESSION['USER']['userid'], $_POST['product_id']);
            $this->view('dashboard/list-products-open', ['page_title'=>'Dashboard - Product List', 'products'=>$this->products, 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms,'msg'=>'We have sent your request to review product: <strong>'.$_POST['product_name'].'</strong>. You will be notified shortly whether your request has been successful.', 'msg-class'=>'success']);
        }
        if ($_SESSION['USER']['role']=='admin') {
            $this->view('dashboard/list-products', ['page_title'=>'Dashboard - Product List', 'products'=>$this->products, 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms]);
        }

        if ($_SESSION['USER']['role']=='influencer') {
            $this->view('dashboard/list-products-open', ['page_title'=>'Dashboard - Product List', 'products'=>$this->products, 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms]);
        }
    }

    public function add_product() {
        
        if (isset($_POST['add_product_btn']) && isset($_POST['product_name']) && isset($_POST['product_url'])) {

            //print_r($_POST);exit;

            //checknumber of reviews is a positive number 
            if (isset($_POST['num_reviews_wanted']) && $_POST['num_reviews_wanted'] < 1) {
                $this->view('dashboard/add-product', ['page_title'=>'Dashboard - Add Product', 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms, 'msg'=>'Invalid number for \'Number of reviews needed\'. You cannot have '.$_POST['num_reviews_wanted'].' reviews. You must enter 1 or more per product.','msg-class'=>'danger']);
                exit;
            }

            //check number of reviews wanted
            if (isset($_POST['num_reviews_wanted']) && $_POST['num_reviews_wanted'] > NUM_REVIEWS_ALLOWED) {
                $this->view('dashboard/add-product', ['page_title'=>'Dashboard - Add Product', 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms, 'msg'=>'You cannot have '.$_POST['num_reviews_wanted'].' reviews. You account is restricted to '.NUM_REVIEWS_ALLOWED.' per product.','msg-class'=>'danger']);
                exit;
            }    

            $product_name_clean = str_replace(' ', '-', $_POST['product_name']); // Replaces all spaces with hyphens.
            $product_name_clean = preg_replace('/[^A-Za-z0-9\-]/', '', $product_name_clean); // Removes special chars.            
            $product_image_filename = 'product_' . $product_name_clean . '_' . time() . '.jpg';
            
            if ($product_id = $this->dash->add_product($_SESSION['USER']['userid'],$_POST['product_name'],$_POST['product_url'],$_POST['product_notes'], $_POST['num_reviews_wanted'], $_POST['product_cat_id'], $_POST['website_platform_id'],$product_image_filename)) {
                
                if (isset($_FILES['product_image'])) {                    
                    $source_file = $_FILES['product_image']['tmp_name'];
                    $dest_file = PRODUCT_IMGS_PATH . $product_image_filename;
                    $this->dash->resize_image($_FILES['product_image']['tmp_name'], $dest_file, 400, 400);
                    move_uploaded_file($source_file, $dest_file) or die ("Could not upload file.");
                }

                $this->view('dashboard/add-product', ['page_title'=>'Dashboard - Add Product', 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms, 'msg'=>'You have successfully added a product!', 'msg-class'=>'success']);                   
            } else {                
                $this->view('dashboard/add-product', ['page_title'=>'Dashboard - Add Product', 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms, 'msg'=>'There was a problem adding your product.','msg-class'=>'danger']);
            }
        }
        
        $this->view('dashboard/add-product', ['page_title'=>'Dashboard - Add Product', 'product_categories'=>$this->product_categories, 'website_platforms'=>$this->website_platforms, 'msg'=>'','msg-class'=>'']);
    }

    public function edit_product($product_id) {

        if (!is_numeric($product_id)) {
            $this->show_error_404();
        }

        //check product exists; this will come later: and is 'owned' by logged in user
        if (!$this->dash->product_exists($product_id)) {
            $this->show_error_404();
        }      
        
        if (isset($_POST['edit_product_btn']) && isset($_POST['product_name']) && isset($_POST['product_url'])) {

            $product_image_filename = $_POST['product_image_filename'];

            if (isset($_FILES['product_image']) && $_FILES['product_image']['tmp_name']!='') {
                $product_image_filename = 'product_' . $product_id . '_' . time() . '.jpg';
                $source_file = $_FILES['product_image']['tmp_name'];
                $dest_file = PRODUCT_IMGS_PATH . $product_image_filename;
                $this->dash->resize_image($_FILES['product_image']['tmp_name'], $dest_file, 400, 400);
                move_uploaded_file($source_file, $dest_file) or die ("Could not upload file.");
            }
            
            if ($this->dash->edit_product($_SESSION['USER']['userid'],$product_id,$_POST['product_name'],$_POST['product_url'],$_POST['product_notes'],$_POST['num_reviews_wanted'],$_POST['product_cat_id'], $_POST['website_platform_id'],$product_image_filename)) {                
                $this->view('dashboard/edit-product', ['page_title'=>'Dashboard - Edit Product', 'product_categories'=>$this->product_categories,'product'=>$this->dash->get_product_by_id($product_id),'website_platforms'=>$this->website_platforms, 'msg'=>'You have successfully edited the product!', 'msg-class'=>'success']);                   
            } else {
                $this->view('dashboard/edit-product', ['page_title'=>'Dashboard - Edit Product', 'product_categories'=>$this->product_categories,'product'=>$this->dash->get_product_by_id($product_id),'website_platforms'=>$this->website_platforms, 'msg'=>'There was a problem editing your product.','msg-class'=>'danger']);
            }
        }
        
        $this->view('dashboard/edit-product', ['page_title'=>'Dashboard - Edit Product', 'product_categories'=>$this->product_categories,'product'=>$this->dash->get_product_by_id($product_id),'website_platforms'=>$this->website_platforms]);
    }

    public function view_products($category_id=false,$category_slug=false) {        
        /*
        if ($product_id) {
            $this->view('dashboard/view-products', ['page_title'=>'Dashboard - View Products', 'product_categories'=>$this->product_categories,'product'=>$this->dash->get_product_by_id($product_id),'website_platforms'=>$this->website_platforms]);
        }*/
        if ($category_id) {

            if (!is_numeric($category_id)) {
                $this->show_error_404();
            }

            //bit of a kludge here but need to check cat id is valid
            if ($category_id >= 1 && $category_id <= 25) {
                
                $this->website_platforms = $this->dash->get_website_platforms();
                $this->view('dashboard/view-products', ['page_title'=>'Dashboard - View Products', 'product_categories'=>$this->product_categories,'products'=>$this->dash->get_products_by_cat_id($category_id),'website_platforms'=>$this->website_platforms]);
            } else {
                $this->show_error_404();
            }         
        } else {
            $this->show_error_404();
        }
    }

    /***
     * 
     * REVIEWS
     * 
     ***/     

     public function list_reviews() {            
        if ($_SESSION['USER']['role']=='admin') {
            $reviews = $this->dash->get_reviews_by_user($_SESSION['USER']['userid']);
            $this->view('dashboard/list-reviews', ['page_title'=>'Dashboard - My Reviews List', 'reviews'=>$reviews]);
        }

        if ($_SESSION['USER']['role']=='influencer') {
            $reviews = $this->dash->get_reviews_by_influencer($_SESSION['USER']['userid']);
            $this->view('dashboard/list-reviews', ['page_title'=>'Dashboard - My Reviews List', 'reviews'=>$reviews]);
        } 
    }

    public function add_review() {            

        if (isset($_POST['add_review_btn']) && isset($_POST['review_url'])) {
  
            //ADD A CHECK IN HERE SO THEY CAN'T REFRESH AND ADD REVIEW TO SAME PRODUCT!
            if ($this->dash->add_review($_POST['user_id'],$_POST['influencer_id'], $_POST['product_id'], $_POST['website_platform_id'], $_POST['review_url'], $_POST['notes'])) {                               
                //$this->dash->incremenet_review_count($product_id);
                $reviews = $this->dash->get_reviews_by_influencer($_SESSION['USER']['userid']);
                $this->view(
                    'dashboard/list-reviews',
                    ['page_title'=>'My Reviews List',
                    'reviews'=>$reviews,
                    'msg'=>'You have successfully added a review! The product owner will be notified by email.',
                    'msg-class'=>'success']
                );                   
                exit;
            } else {                
                $this->view('dashboard/add-review', ['page_title'=>'Dashboard - Add Review', 'msg'=>'There was a problem adding your review.','msg-class'=>'danger']);
            }
        }    
        
        if (isset($_POST['submit_review_btn']) && isset($_POST['product_id']) && isset($_POST['product_name'])){
            $this->view('dashboard/add-review', ['page_title'=>'Dashboard - Add Review','website_platforms'=>$this->website_platforms,'request'=>$_POST]);
        } else {
            $this->view('home/index', ['page_title'=>'Home']);
            exit;
        }
        
    }
    /*
    public function edit_review($review_id) {
          
   
        if (isset($_POST['edit_review_btn']) && isset($_POST['review_url'])) {
            if ($this->dash->edit_review($_SESSION['USER']['userid'], $_POST['website_platform_id'],$_POST['review_url'],$review_id,$_POST['notes'])) {                
                $this->view('dashboard/view-review', ['page_title'=>'Dashboard - Edit Review','website_platforms'=>$this->website_platforms, 'review'=>$this->dash->get_review_by_id($review_id), 'msg'=>'You have successfully edited the review!','msg-class'=>'success']);                   
            } else {
                $this->view('dashboard/view-review', ['page_title'=>'Dashboard - Edit Review','website_platforms'=>$this->website_platforms, 'review'=>$this->dash->get_review_by_id($review_id), 'msg'=>'There was a problem editing your review.','msg-class'=>'danger']);
            }
        }        
        $this->view('dashboard/view-review', ['page_title'=>'Dashboard - Edit Review','website_platforms'=>$this->website_platforms,'review'=>$this->dash->get_review_by_id($review_id)]);
    }
*/
    public function view_review($review_id) {

        if (!is_numeric($review_id)) {
            $this->show_error_404();
        }

        //check review exists; this will come later: and is 'owned' by logged in user
        if (!$this->dash->review_exists($review_id)) {
            $this->show_error_404();
        }

        $this->website_platforms = $this->dash->get_website_platforms();
        
        $review = $this->dash->get_review_by_id($review_id);
        $product = $this->dash->get_product_by_id($review[0]['product_id']); 
        $influencer = $this->dash->get_influencer_by_id($review[0]['influencer_id']);              
        $this->view('dashboard/view-review', ['page_title'=>'Dashboard - View Review','website_platforms'=>$this->website_platforms,'review'=>$review,'product'=>$product, 'product_categories'=>$this->product_categories,'influencer'=>$influencer]);
    }

    /***
     * 
     * REQUESTS
     * 
     ***/

    public function list_requests($view='table', $category='all') {   
        
        if ($_SESSION['USER']['role']=='admin') {
            $requests = $this->requests->get_requests_by_user($_SESSION['USER']['userid']);
            $this->view('dashboard/list-requests', ['page_title'=>'Dashboard - Requests List','website_platforms'=>$this->website_platforms, 'requests'=>$requests]);
        }

        if ($_SESSION['USER']['role']=='influencer') {
            $requests = $this->requests->get_requests_by_influencer($_SESSION['USER']['userid']);
            $this->view('dashboard/list-requests-open', ['page_title'=>'Dashboard - Requests List','website_platforms'=>$this->website_platforms, 'requests'=>$requests]);
        }
    }

    public function view_request($request_id) {

        if (!is_numeric($request_id)) {
            $this->show_error_404();
        }

        //check request exists; this will come later: and is 'owned' by logged in user
        if (!$this->requests->request_exists($request_id)) {
            $this->show_error_404();
        }        

        if ($_SESSION['USER']['role']=='admin') {                        
                      
            if (isset($_POST['update_request_status_btn']) && isset($_POST['review_request_id']) && isset($_POST['request_status'])) {
                $this->requests->update_status($_POST['review_request_id'],array('request_status',$_POST['request_status']), array('request_status_notes',$_POST['request_status_notes']));
                $requests = $this->requests->get_requests_by_user($_SESSION['USER']['userid'],$request_id);
                $this->view('dashboard/view-request', ['page_title'=>'Dashboard - View Request', 'product_categories'=>$this->product_categories,'website_platforms'=>$this->website_platforms, 'request'=>$requests,'msg'=>'You have successfully updated the request status.', 'msg-class'=>'success']);
                exit;
            } 
            if (isset($_POST['update_product_status_btn']) && isset($_POST['review_request_id']) && isset($_POST['product_status'])) {
                $this->requests->update_status($_POST['review_request_id'],array('product_status',$_POST['product_status']), array
                ('product_status_notes',$_POST['product_status_notes']), array('product_sent_date',$_POST['product_sent_date']));
                $requests = $this->requests->get_requests_by_user($_SESSION['USER']['userid'],$request_id);
                $this->view('dashboard/view-request', ['page_title'=>'Dashboard - View Request', 'product_categories'=>$this->product_categories,'website_platforms'=>$this->website_platforms, 'request'=>$requests,'msg'=>'You have successfully updated the product status.', 'msg-class'=>'success']);
                exit;
            }
            if (isset($_POST['update_payment_status_btn']) && isset($_POST['review_request_id']) && isset($_POST['payment_status'])) {
                $this->requests->update_status($_POST['review_request_id'],array('payment_status',$_POST['payment_status']), array('payment_status_notes',$_POST['payment_status_notes']), array('payment_sent_date',$_POST['payment_sent_date']));
                $requests = $this->requests->get_requests_by_user($_SESSION['USER']['userid'],$request_id);
                $this->view('dashboard/view-request', ['page_title'=>'Dashboard - View Request', 'product_categories'=>$this->product_categories,'website_platforms'=>$this->website_platforms, 'request'=>$requests,'msg'=>'You have successfully updated the payment status.', 'msg-class'=>'success']);
                exit;
            }                       
            if (isset($_POST['accept_request_btn']) && isset($_POST['review_request_id']) && isset($_POST['user_id'])) {
                if ($this->requests->check_request_accepted($request_id)) {
                    $this->view('dashboard/view-request', ['page_title'=>'Dashboard - View Request', 'product_categories'=>$this->product_categories,'website_platforms'=>$this->website_platforms, 'request'=>$requests,'msg'=>'You have already accepted this request. The influencer will be notified by email.', 'msg-class'=>'danger']);
                    exit;
                }
                $this->requests->accept_request($_POST['user_id'], $_POST['review_request_id']);
                $requests = $this->requests->get_requests_by_user($_SESSION['USER']['userid'],$request_id);
                $this->view('dashboard/view-request', ['page_title'=>'Dashboard - View Request', 'product_categories'=>$this->product_categories,'website_platforms'=>$this->website_platforms, 'request'=>$requests,'msg'=>'You have successfully accepted this review request. The influencer will be notified by email.', 'msg-class'=>'success']);
                exit;
            }          
            $requests = $this->requests->get_requests_by_user($_SESSION['USER']['userid'],$request_id);
            $this->view('dashboard/view-request', ['page_title'=>'Dashboard - View Request', 'product_categories'=>$this->product_categories,'website_platforms'=>$this->website_platforms, 'request'=>$requests]);
        }
/*
        if ($_SESSION['USER']['role']=='influencer') {
            $requests = $this->requests->get_requests_by_influencer($_SESSION['USER']['userid'],$request_id);
            $this->view('dashboard/view-request-open', ['page_title'=>'Dashboard - View Request','website_platforms'=>$this->website_platforms, 'requests'=>$requests]);
        } 
        */
    }

    /***
     * 
     * USERS
     * 
     ***/     

     public function list_influencers($influencer_id=false) {
         
        if ($_SESSION['USER']['role']=='admin') {            
            $this->website_platforms = $this->dash->get_website_platforms(); 
            if ($influencer_id) {

                if (!is_numeric($influencer_id)) {
                    $this->show_error_404();
                }
        
                //check influencer exists; this will come later: and is 'owned' by logged in user
                if (!$this->dash->influencer_exists($influencer_id)) {
                    $this->show_error_404();
                }

                $influencers = $this->dash->get_influencer_by_id($influencer_id);
            } else {
                $influencers = $this->dash->get_influencers($_SESSION['USER']['userid']);
            }
            $this->view('dashboard/list-influencers-open', ['page_title'=>'Dashboard - Influencers', 'website_platforms'=>$this->website_platforms, 'influencers'=>$influencers]);
        } else {
            $this->view('home/index', ['page_title'=>'Home']);
            exit;
        }
    }

    /***
     * 
     * PRODUCTS
     * 
     ***/     

    public function list_sent_products() {         
        if ($_SESSION['USER']['role']=='admin') {               
            $this->view('dashboard/list-sent-products', ['page_title'=>'Dashboard - Products Sent']);
        } else {
            $this->show_error_404();
        }
    }

    /***
     * 
     * PAYMENTS
     * 
     ***/     

    public function list_payments() {         
        if ($_SESSION['USER']['role']=='admin') {               
            $this->view('dashboard/list-payments', ['page_title'=>'Dashboard - Payments']);
        } else {
            $this->show_error_404();
        }
    }



}