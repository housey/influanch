<?php

class Payment extends Controller
{

    public function index () {
        
    }

    public function ipn () {
        require_once PAYPAL_PATH.'ipn.php';
    }

    public function payment_success () {
        $this->view('payment/payment-success', ['page_title'=>'Payment Success']);
    }

    public function cancel_payment () {
        $this->view('payment/cancel-payment', ['page_title'=>'Cancel Payment']);
    }

}