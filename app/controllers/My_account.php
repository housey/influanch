<?php

class My_account extends Controller
{
    public $user_data, $num_pending_requests;

    public function __construct() {
        $this->myaccount = $this->model('myaccountmodel');    
        $this->requests = $this->model('requests');
        $this->user_data = $this->myaccount->get_user_profile($_SESSION['USER']['userid']);
        $this->num_pending_requests = $this->requests->get_num_pending_requests($_SESSION['USER']['userid']); 
        $this->num_accepted_requests = $this->requests->get_num_accepted_requests($_SESSION['USER']['userid']);
    }

    public function index() {
        $this->refuse_non_authed();    
        $user_platform_data = $this->myaccount->get_user_platforms($_SESSION['USER']['userid']);        
        $this->view('my-account/index', ['page_title'=>'My Account','user_data'=>$this->user_data, 'num_pending_requests'=>$this->num_pending_requests,'num_accepted_requests'=>$this->num_accepted_requests,'user_platform_data'=>$user_platform_data]); 
    }
}