<?php

class Profile extends Controller
{

    public function __construct() {
        $this->refuse_non_authed();            
        $this->profile = $this->model('Myaccountmodel');  
        $this->user_data = $this->profile->get_user_profile($_SESSION['USER']['userid']);
        $this->user_platform_data = $this->profile->get_user_platforms($_SESSION['USER']['userid']);
    }

    public function index($user_id='') {

        if (empty($this->user_platform_data)) {
            $this->user_platform_data = [
                'amazon'=>'',
                'blog'=>'',
                'facebook'=>'',
                'instagram'=>'',
                'linkedin'=>'',
                'pinterest'=>'',
                'podcast'=>'',
                'soundcloud'=>'',
                'twitter'=>'',
                'youtube'=>''
            ];
        }
        
        if (isset($_POST['edit_user_btn']) && isset($_POST['firstname']) && isset($_POST['password'])) {
            $profile_image_filename = $_POST['profile_image_filename'];

            if (isset($_FILES['profile_image']) && $_FILES['profile_image']['tmp_name']!='') {
                $profile_image_filename = 'profile_' . $_SESSION['USER']['userid'] . '_' . time() . '.jpg';
                $source_file = $_FILES['profile_image']['tmp_name'];
                $dest_file = PROFILE_IMGS_PATH . $profile_image_filename;
                $this->dashboard->resize_image($_FILES['profile_image']['tmp_name'], $dest_file, 300, 300);
                move_uploaded_file($source_file, $dest_file) or die ("Could not upload file.");
            }
      
            /* check mobile is a number */
            if (isset($_POST['mobile']) && $_POST['mobile']!='' && !is_numeric($_POST['mobile'])) {
                $this->view('profile/index', ['page_title'=>'Profile','user_data'=>$this->user_data,'user_platform_data'=>$this->user_platform_data, 'msg'=>'Mobile is not a number.', 'msg-class'=>'danger']);                   
            }

            /* check passwords match */
            if ($_POST['confirm-password']!=$_POST['password']) {
                $this->view('profile/index', ['page_title'=>'Profile','user_data'=>$this->user_data,'user_platform_data'=>$this->user_platform_data, 'msg'=>'The passwords don\'t match. Using old password.','msg-class'=>'danger']);
            }
            
            $categories = implode(',', $_POST['categories']);
            $_SESSION['USER']['categories'] = $categories;            
                    
            if ($this->profile->edit_user_profile(
                $_SESSION['USER']['userid'],
                $_POST['firstname'],
                $_POST['lastname'],
                $_POST['password'],
                $_POST['address'],
                $_POST['postcode'],
                $_POST['country'],
                $_POST['mobile'],
                $_POST['paypal_email'],
                $_POST['bio'],
                $profile_image_filename,
                $categories
                )) {                

                    $website_platforms = [
                        'amazon'=>$_POST['amazon'],
                        'blog'=>$_POST['blog'],
                        'facebook'=>$_POST['facebook'],
                        'instagram'=>$_POST['instagram'],
                        'linkedin'=>$_POST['linkedin'],
                        'pinterest'=>$_POST['pinterest'],
                        'podcast'=>$_POST['podcast'],
                        'soundcloud'=>$_POST['soundcloud'],
                        'twitter'=>$_POST['twitter'],
                        'youtube'=>$_POST['youtube']
                    ];

                    $this->profile->update_user_platforms($_SESSION['USER']['userid'],$website_platforms);                

                    $this->user_data = $this->profile->get_user_profile($_SESSION['USER']['userid']);
                    $this->user_platform_data = $this->profile->get_user_platforms($_SESSION['USER']['userid']);
                    $this->view('profile/index', ['page_title'=>'Profile','user_data'=>$this->user_data,'user_platform_data'=>$this->user_platform_data, 'msg'=>'You have successfully updated your profile!', 'msg-class'=>'success']);                   
                    } else {
                        $this->view('profile/index', ['page_title'=>'Profile','user_data'=>$this->user_data,'user_platform_data'=>$this->user_platform_data, 'msg'=>'There was a problem updating your profile.','msg-class'=>'danger']);
                }
        }        
            $this->view('profile/index', ['page_title'=>'Profile','user_data'=>$this->user_data,'user_platform_data'=>$this->user_platform_data]);
    }

}