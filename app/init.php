<?php

global $current_host;
$current_host = 'DEV'; //LIVE

define('TABLE_USER', 'rrc_users');
define('TABLE_USER_WEBSITE_PLATFORMS', 'rrc_user_website_platforms'); 
define('TABLE_PRODUCTS', 'rrc_products');
define('TABLE_PRODUCT_CATEGORIES', 'rrc_product_categories');
define('TABLE_REQUESTS', 'rrc_review_requests');
define('TABLE_REVIEWS', 'rrc_reviews');
define('TABLE_WEBSITE_PLATFORMS', 'rrc_website_platforms');

define('TABLE_PAYPAL_PAYMENTS', 'rrc_paypal_payments');
define('TABLE_PAYMENTS', 'rrc_payments');

define('MAIL_SENDER_EMAIL', 'tim@timd.co.uk');
define('MAIL_SENDER_NAME', 'Influlaunch');

define('SITE_ADMIN_NAME', 'Tim');
define('SITE_ADMIN_EMAIL', 'abstractia@gmail.com');

if ($current_host == 'DEV') {
    define('APP_URL', 'http://localhost:8080/real-reviews-cms/public/');
    //define('ABSPATH', dirname(__FILE__));
    define('ABSPATH', dirname(dirname(dirname(__FILE__))));
    define('SUB_DIR', '/real-reviews-cms/');
    define('BASE_URL', "https://" . $_SERVER['HTTP_HOST'] . SUB_DIR);
    define('DOC_PATH', $_SERVER['DOCUMENT_ROOT'] . SUB_DIR);
    define('APP_PATH', DOC_PATH.'app/');
    define('VIEWS_PATH', APP_PATH.'views/');
    define('TPLATES_PARTS_PATH', VIEWS_PATH.'template_parts/');
    define('PUBLIC_PATH', DOC_PATH.'public/');
    define('VENDOR_PATH', PUBLIC_PATH.'vendor/');
    define('PAYPAL_PATH', PUBLIC_PATH.'vendor/paypal/');
    define('PAYPAL_DIR_URL', APP_URL.'vendor/paypal/');
    define('PRODUCT_IMGS_URL', APP_URL.'uploads/product_images/');
    define('PROFILE_IMAGES_URL', APP_URL.'uploads/profile_images/');
    define('PRODUCT_IMGS_PATH', PUBLIC_PATH.'uploads/product_images/');
    define('PROFILE_IMGS_PATH', PUBLIC_PATH.'uploads/profile_images/');
}

if ($current_host == 'LIVE') {
    define('APP_URL', 'https://timd.co.uk/influlaunch/');
    //define('ABSPATH', dirname(__FILE__));
    define('ABSPATH', dirname(dirname(dirname(__FILE__))));
    define('SUB_DIR', '/influlaunch/');
    define('BASE_URL', "https://" . $_SERVER['HTTP_HOST'] . SUB_DIR);
    define('DOC_PATH', $_SERVER['DOCUMENT_ROOT'] . SUB_DIR);
    define('APP_PATH', '/home4/bangsara/app/');
    define('VIEWS_PATH', APP_PATH.'views/');
    define('TPLATES_PARTS_PATH', VIEWS_PATH.'template_parts/');
    define('PUBLIC_PATH', '/home4/bangsara/public_html/timdco/influlaunch/');
    define('VENDOR_PATH', PUBLIC_PATH.'vendor/');
    define('PAYPAL_PATH', PUBLIC_PATH.'vendor/paypal/');
    define('PAYPAL_DIR_URL', APP_URL.'vendor/paypal/');
    define('PRODUCT_IMGS_URL', APP_URL.'uploads/product_images/');
    define('PROFILE_IMAGES_URL', APP_URL.'uploads/profile_images/');
    define('PRODUCT_IMGS_PATH', PUBLIC_PATH.'uploads/product_images/');
    define('PROFILE_IMGS_PATH', PUBLIC_PATH.'uploads/profile_images/');
}
/*
echo ABSPATH.' <--- <br />';
echo APP_URL.' <br />';
echo SUB_DIR.' <br />';
echo BASE_URL.' <br />';
echo DOC_PATH.' <br />';
echo APP_PATH.' <br />';
echo VIEWS_PATH.' <br />';
echo TPLATES_PARTS_PATH.' <br />';
*/
define('NUM_REVIEWS_ALLOWED', 100); //this is per review
define('COOKIE_NAME', 'Influlaunch');

$data['page_title']='';
spl_autoload_register(function ($class_name) {
    require_once APP_PATH.'classes/' . $class_name . '.class.php';
});

require_once 'core/App.php';
require_once 'core/Controller.php';

$db = new db();
$system = new System();
$user = new User();

//print_r($system);
