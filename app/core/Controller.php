<?php

class Controller
{

    public function __construct () {
    }

    public function refuse_non_authed() {
        if (!isset($_SESSION['USER']) || $_SESSION['USER']['loggedin']==0){
            header('Location: '.APP_URL); 
        }  
    }

    public function model($model) {
        require_once APP_PATH . 'models/' . ucfirst($model) . '.php';
        return new $model();
    }

    public function view($view,$data=[]) {
        if (file_exists(APP_PATH . 'views/' . $view . '.php')) {
            require_once APP_PATH . 'views/' . $view . '.php';
        } else {
            $this->show_error_404();
        }
    }    

    public function show_error_404() {
        $this->view('error-404/index',['msg'=>'Error 404: Page Not Found.','msg-class'=>'danger']);
        exit;
    }    
    
}