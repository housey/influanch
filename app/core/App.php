<?php

class App
{

    protected $controller = 'home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
    {
        // Get broken up URL
        $url = $this->parseUrl();
  
        // Does the requested controller exist?
        // If so, set it and unset from URL array
        //do string replace so hyphens can be used in URLs
        $url_part_zero = str_replace('-', '_', $url[0]);
        if (file_exists(APP_PATH . 'controllers/' . ucfirst($url_part_zero) . '.php')) {
            $this->controller = $url_part_zero;
            unset($url[0]);
        }
        require_once APP_PATH . 'controllers/' . ucfirst($this->controller) . '.php';
        $this->controller = new $this->controller();
        // Has a second parameter been passed?
        // If so, it might be the requested method
        if (isset($url[1])) {
            //do string replace so hyphens can be used in URLs
            $url_part_one = str_replace('-', '_', $url[1]);
            if (method_exists($this->controller, $url_part_one)) {
                $this->method = $url_part_one;
                unset($url[1]);
            }
        }
        // Set parameters to either the array values or an empty array
        $this->params = $url ? array_values($url) : [];
        // Call the chosen method on the chosen controller, passing
        // in the parameters array (or empty array if above was false)
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    protected function parseUrl() {
        if (isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }

}