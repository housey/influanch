<?php 

class Register
{

    public $db;
    public $user;

    public function __construct() {
        $this->db = new DB();
        $this->user = new User();
    }

    public function signup_user() {
        
        $this->user->firstname = $this->db->mysqli->real_escape_string($_POST['firstname']);
        $this->user->lastname = $this->db->mysqli->real_escape_string($_POST['lastname']);
        $this->user->email = $this->db->mysqli->real_escape_string($_POST['email']);
        $this->user->password = $this->db->mysqli->real_escape_string($_POST['password']);
        $categories = implode(',', $_POST['categories']);        
        $this->user->categories = $categories;
        $this->user->role = 'influencer';

        //Create a "unique" token.
        $this->user->token = bin2hex(openssl_random_pseudo_bytes(16));

        if ($this->user->user_exists($this->user->email) != '') {
            return false;
        } else {
            $this->user->save_user($this->user->password, false);            
            $user_id = $this->user->user_exists($this->user->email);

            //insert row in user_website_platforms table for this user
            $sql = "INSERT INTO ".TABLE_USER_WEBSITE_PLATFORMS." (user_id) VALUES ('$user_id')";
            $this->db->query($sql);
            $website_platforms_id = $this->db->get_insert_id();
            $sql = "UPDATE ".TABLE_USER." SET user_website_platform_id='$website_platforms_id' WHERE  user_id=$user_id";
            $this->db->query($sql);

            //Construct the URL.
            $verify_url = BASE_URL . "users/verify-user/" . $this->user->token . "/" . $user_id . "/";
            $verify_link = '<a href="'.$verify_url.'">'.$verify_url.'</a>';
            
            $mailer = new Sysmail();
            $verify_button = $mailer->get_button($verify_url, 'Verify Sign Up');
            $mailer->send_email($_POST['firstname'], $_POST['email'], 
            "Welcome To Influlaunch, Verify Sign Up.", 
            "Welcome to Influlaunch. Please click the button below to verify your sign up or copy and paste this link: ".$verify_url." <br /> <br />".$verify_button." <p>Kind Regards,</p> <p> Influlaunch</p>");

            return true;
        }
    }

    public function user_exists($email) {
        return $this->user->user_exists($email);
    }

    public function check_user_token_verified($token, $user_id) {
        $sql = "SELECT * FROM ".TABLE_USER." WHERE user_id = " . $user_id . " AND token = '" . $token . "' AND verified = 'N' LIMIT 1 ";
        $result = $this->db->query_2_array($sql);
        return $result;
    }

    public function verify_user($token, $user_id) {
        
        $verified = false;
        $result = $this->check_user_token_verified($token, $user_id);        
        $recCount = count($result);

        if ($recCount == 1) {
            if ($result[0]['verified'] == 'N') {
                $sql = "UPDATE ".TABLE_USER." SET verified='Y' WHERE  user_id = " . $user_id;
                $this->db->query($sql);
                $verified = true;
            } else {
                $verified = true;
            }
        } else {
            $verified = false;
        }
        return $verified;
    }

    public function send_welcome_mail($user_id) {

        $user_arr = $this->user->get_user($user_id);
        
        $mailer = new Sysmail();
        $mailer->send_email($user_arr[0]['firstname'], $user_arr[0]['email'], 
        "Welcome To Influlaunch, Account Verified!", 
        '<h2>Welcome to Influlaunch</h2> <p>We will be sending you lots of products to review.</p><p>You may now login by going <a href="'.BASE_URL.'users/login">here</a></p><p>Kind Regards,</p><p>Influlaunch</p>');
    }
    
}
