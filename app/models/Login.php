<?php 

class Login
{
    public $user;
    public $db;

    public function __construct() {
        $this->user = new User();
        $this->db = new DB();        
    }

    public function login_user($email = '', $password = '') {
        $this->user->login($email, $password);
    }

    public function check_user_verified($user_id) {

        $verified = false;
        $sql = "SELECT * FROM ".TABLE_USER." WHERE user_id = " . $user_id . " LIMIT 1 ";
        
        $result = $this->db->query_2_array($sql);
        if ($result[0]['verified'] == 'Y') {
            $verified = true;
        } else {
            $verified = false;
        }
        return $verified;
    }

    public function logout() {
        //$this->user->logout();
        
        // Initialize the session.
        //session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();
    }
    
}
