<?php

class Requests 
{

    public $db;
    public $product_category;
    public $product_name;

    public function __construct () {
        $this->db = new DB();
        $this->user = new User();
    }

    public function get_num_pending_requests($user_id) {
        $sql = "SELECT COUNT(*) AS count FROM ".TABLE_REQUESTS." WHERE user_id = {$user_id} AND request_accepted = 'N'";
        $result = $this->db->query_2_array($sql);
        $_SESSION['USER']['num_pending_requests'] = $result[0]['count'];
        return $result[0]['count'];
    }

    public function get_num_accepted_requests($influencer_id) {
        $sql = "SELECT COUNT(*) AS count FROM ".TABLE_REQUESTS." WHERE influencer_id = {$influencer_id} AND request_accepted = 'Y' AND review_complete = 'N'";
        $result = $this->db->query_2_array($sql);        
        $_SESSION['USER']['num_accepted_requests'] = $result[0]['count'];
        return $result[0]['count'];
    }

    public function request_exists($request_id) {
        $sql = "SELECT * FROM ".TABLE_REQUESTS." WHERE review_request_id=".$request_id." LIMIT 1";
        $result = $this->db->query_2_array($sql);
        return count($result);
    }

    public function get_requests_by_influencer($influencer_id){
        $sql = "SELECT *  FROM ".TABLE_REQUESTS."
INNER JOIN ".TABLE_PRODUCTS." ON ".TABLE_REQUESTS.".product_id = ".TABLE_PRODUCTS.".product_id
INNER JOIN ".TABLE_PRODUCT_CATEGORIES." ON ".TABLE_PRODUCT_CATEGORIES.".product_cat_id = ".TABLE_PRODUCTS.".product_cat_id WHERE ".TABLE_REQUESTS.".influencer_id = ".$influencer_id." ORDER BY review_request_id DESC";
        $result = $this->db->query_2_array($sql);
        return $result;
    }

    public function get_requests_by_user($user_id,$request_id=false,$order_by=" ORDER BY ".TABLE_REQUESTS.".request_date_added DESC"){
       
        $sql = "SELECT * FROM ".TABLE_REQUESTS."
INNER JOIN ".TABLE_PRODUCTS." ON ".TABLE_REQUESTS.".product_id = ".TABLE_PRODUCTS.".product_id
INNER JOIN ".TABLE_PRODUCT_CATEGORIES." ON ".TABLE_PRODUCT_CATEGORIES.".product_cat_id = ".TABLE_PRODUCTS.".product_cat_id 
INNER JOIN ".TABLE_USER." ON ".TABLE_USER.".user_id = ".TABLE_REQUESTS.".influencer_id ";

        if ($request_id!=false) {
            $sql .= "WHERE ".TABLE_REQUESTS.".review_request_id = ".$request_id;
            $sql .= $order_by;
            $result = $this->db->query_2_array($sql);
            $user_platform_data = $this->get_user_platforms($result[0]['influencer_id']);
            $result = array_merge($result,$user_platform_data);
        } else {
            $sql.="WHERE ".TABLE_REQUESTS.".user_id = ".$user_id;
            $sql.=$order_by;
            $result = $this->db->query_2_array($sql);
        }
        
        return $result;
    }

    //move this method - also exists in MyaccountModel model class!
    public function get_user_platforms($userid) {        
        $sql = "SELECT * FROM ".TABLE_USER_WEBSITE_PLATFORMS." WHERE user_id=".$userid." LIMIT 1";         
        $user_platform_data = $this->db->query_2_array($sql);
        return $user_platform_data;      
    }

    public function check_user_request($influencer_id, $product_id) {
        $sql = "SELECT * FROM ".TABLE_REQUESTS." WHERE product_id = ".$product_id." AND influencer_id = ".$influencer_id." LIMIT 1";                
        $result = $this->db->query_2_array($sql);
        if (!empty($result)){
            return true;
        } else {
            return false;
        }
    }

    public function get_request_by_id($review_request_id) {
        $sql = "SELECT * FROM ".TABLE_REQUESTS." WHERE review_request_id = ".$review_request_id;
        $result = $this->db->query_2_array($sql);
        return $result;
    }    
    
    public function add_request($user_id, $influencer_id, $product_id, $notes='') {      
        //$date_added = date('Y-m-d');
        $sql = "INSERT INTO ".TABLE_REQUESTS." (user_id, influencer_id, product_id) VALUES ('$user_id', '$influencer_id', '$product_id')";
        $this->db->query($sql);

        $sql = "SELECT * FROM ".TABLE_USER." WHERE user_id = ".$user_id." LIMIT 1";
        $user_brand = $this->db->query_2_array($sql);
        
        $mailer = new Sysmail();

        //email BRAND
        $mailer->send_email($user_brand[0]['firstname'], $user_brand[0]['email'], 
        "Influlaunch: Someone requested to review your product", 
        "Someone has requested to review your product, please login. <p>Kind Regards,</p> <p> Influlaunch</p>");

        //email INFLUENCER        
        $mailer->send_email($_SESSION['USER']['firstname'], $_SESSION['USER']['email'], 
        "Influlaunch: You have requested to review a product", 
        "You have requested to review a product. We will contact you shortly to let you know if you have been accepted or not. <p>Kind Regards,</p> <p> Influlaunch</p>");

        return true;
    }

    public function check_request_accepted($request_id) {
        $sql = "SELECT request_accepted FROM ".TABLE_REQUESTS." WHERE review_request_id = ".$request_id." LIMIT 1";
        $result = $this->db->query_2_array($sql);
        if ($result[0]['request_accepted']=='Y') {
            return true;
        } else {
            return false;
        }
    }    

    public function accept_request($influencer_id, $review_request_id, $notes='') {      
        
        $sql = "UPDATE ".TABLE_REQUESTS." SET request_status='Pending', request_accepted='Y', request_influencer_notified='Y' WHERE review_request_id = ".$review_request_id;        
        $this->db->query($sql);

        $sql = "SELECT * FROM ".TABLE_USER." WHERE user_id = ".$influencer_id." LIMIT 1";
        $influencer = $this->db->query_2_array($sql);
           
        //$verify_url = BASE_URL . "users/verify-user/" . $this->user->token . "/" . $user_id . "/";
        //$verify_link = '<a href="'.$verify_url.'">'.$verify_url.'</a>';
        
        $mailer = new Sysmail();

        //$verify_button = $mailer->get_button($verify_url, 'Verify Sign Up');

        //email BRAND
        /*
        $mailer->send_email($user_brand[0]['firstname'], $user_brand[0]['email'], 
        "Influlaunch: You have accepted a review request", 
        "You have successfully accepted a review request. <p>Kind Regards,</p> <p> Influlaunch</p>");
        */

        //email INFLUENCER        
        $mailer->send_email($influencer[0]['firstname'], $influencer[0]['email'], 
        "Influlaunch: Your product review request has been accepted!", 
        "Your product review request has been accepted. <p>Kind Regards,</p> <p> Influlaunch</p>");

        return true;
    }    
/*
    public function acceptBookRequest($bookID, $userID)
    {
        $sql = "UPDATE " . $this->table . " SET BookRequestAccepted = 'Y', AuthorNotifiedRequest = 'Y', BookRequestPending = 'N' WHERE  BookID = " . $bookID . " AND UserID = " . $userID;
        $this->db->query($sql);
        $sql = "UPDATE dev_lw_Signups SET requestsAccepted = requestsAccepted + 1 WHERE  UserID = " . $userID;
        $this->db->query($sql);
    }*/
    /*
    public function decline_request($influencer_id, $review_request_id, $notes='') {   
    {
        //ReviewerNotifiedAccepted = 'N'
        $sql = "UPDATE " . $this->table . " SET BookRequestDeclined = 'Y', AuthorNotifiedRequest = 'Y', BookRequestPending = 'N' WHERE  BookID = " . $bookID . " AND UserID = " . $userID;
        $this->db->query($sql);
        $sql = "UPDATE dev_lw_Signups SET requestsDeclined = requestsDeclined + 1 WHERE  UserID = " . $userID;
        $this->db->query($sql);
    }*/

    public function get_options ($field='') {
        $sql = "SHOW COLUMNS FROM ".TABLE_REQUESTS;
        $result[0] = $this->db->query_2_array($sql);
        $result = $result[0];
        $type = array_search($field, array_column($result, 'Field'));
        $type = $result[$type]['Type'];
        $output = str_replace("enum('", "", $type);        
        $output = str_replace("')", "", $output);
        // array $results contains the ENUM values
        $results = explode("','", $output);
        return $results;
    }

    //echos $_POST var or other var if not $_POST var not set
    public function echo_field_data ($field, $var) {
        echo (isset($_POST[$field]) ? $_POST[$field] : $var);
    }

    public function update_status($review_request_id,$status,$notes=false,$date=false) {
        $sql = "UPDATE ".TABLE_REQUESTS." SET {$status[0]} = '{$status[1]}' ";        
        if ($notes) $sql .= ", $notes[0] = '{$notes[1]}' ";
        if ($date)  $sql .= ", $date[0] = '{$date[1]}' ";
        $sql .= "WHERE review_request_id = ".$review_request_id;
        return $this->db->query($sql);
    }

    //$transcaction_id=0, $accepted='N', $notified='N', $influencer_notified='N' WHERE review_id=1
    public function update_request($user_id, $influencer_id, $product_id, $notes='') {
        //$date_added = date('Y-m-d');                
        $sql = "UPDATE ".TABLE_REQUESTS." SET user_id='$user_id', website_platform_id='$website_platform_id', review_url='$review_url', notes='$notes' WHERE  review_request_id=".$review_request_id;        
        return $this->db->query($sql);
    }
    
}
