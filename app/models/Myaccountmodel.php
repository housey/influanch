<?php 

class MyaccountModel extends User
{
    public $db;

    public function __construct () {
       $this->db = new DB();
    }

    public function get_user_profile($userid) {
        $sql = "SELECT * FROM ".TABLE_USER." 
LEFT JOIN ".TABLE_USER_WEBSITE_PLATFORMS." ON ".TABLE_USER.".user_website_platform_id = ".TABLE_USER_WEBSITE_PLATFORMS.".user_website_platform_id
WHERE ".TABLE_USER.".user_id=".$userid;
        $user_data = $this->db->query_2_array($sql);
        return $user_data;
    }
    
    public function edit_user_profile($userid,$firstname,$lastname,$password,$address,$postcode,$country,$mobile,$paypal_email,$bio,$profile_image_filename,$categories) {
        //add this back in password='".crypt($password, 'x3')."'
        $sql = "UPDATE ".TABLE_USER." SET firstname='$firstname', lastname='$lastname', address='$address',postcode='$postcode',country='$country',mobile='$mobile',paypal_email='$paypal_email',
        bio='$bio',profile_image_filename='$profile_image_filename',categories='$categories' WHERE  user_id=".$userid;
        if ($this->db->query($sql)){
            return true;
        } else {
            return false;
        }
    }
    public function update_user_platforms($userid,$website_platforms) {
        $query_str = '';
        foreach ($website_platforms as $key => $value) {
            $query_str .= $key."="."'$value', ";
        }
        $query_str = rtrim($query_str, ', ');
        $sql = "UPDATE ".TABLE_USER_WEBSITE_PLATFORMS." SET ".$query_str." WHERE user_id=".$userid;
        
        if ($this->db->query($sql)){
            return true;
        } else {
            return false;
        }        
    }

    public function get_user_platforms($userid) {        
        $sql = "SELECT * FROM ".TABLE_USER_WEBSITE_PLATFORMS." WHERE user_id=".$userid." LIMIT 1";       
        $user_platform_data = $this->db->query_2_array($sql);
        return $user_platform_data;      
    }

}
