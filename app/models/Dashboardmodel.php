<?php 

class DashboardModel
{
    public $products;
    public $reviews;
    public $influencers;

    public function __construct () {
        $this->products = new Products();
        $this->reviews = new Reviews();
        $this->influencers = new Influencers();
    }

    public function product_exists($product_id) {
        return $this->products->product_exists($product_id);
    }

    Public function get_products() {
        $product_arr = $this->products->get_products();    
        return $product_arr;
    }

    Public function get_product_by_id($product_id) {
        $product_arr = $this->products->get_product_by_id($product_id);    
        return $product_arr;
    }

    Public function get_products_by_cat_id($category_id) {
        $product_arr = $this->products->get_products_by_cat_id($category_id);    
        return $product_arr;
    }

    Public function get_products_by_user($user_id) {
        $products_arr = $this->products->get_products_by_user($user_id);    
        return $products_arr;
    }

    public function add_product ($user_id, $product_name, $product_url, $product_notes, $num_reviews_wanted, $product_cat_id, $website_platform_id,$product_image_filename) {
        if ($product_id = $this->products->add_product ($user_id, $product_name, $product_url, $product_notes, $num_reviews_wanted, $product_cat_id, $website_platform_id,$product_image_filename)) {
            return $product_id;
        } else {
            return false;
        }        
    }

    public function edit_product ($user_id, $product_id, $product_name, $product_url, $product_notes, $num_reviews_wanted, $product_cat_id, $website_platform_id,$product_image_filename) {
        if ($this->products->edit_product ($user_id, $product_id, $product_name, $product_url, $product_notes,$num_reviews_wanted, $product_cat_id,$website_platform_id,$product_image_filename)) {
            return true;
        } else {
            return false;
        }        
    }    

    Public function get_product_categories() {
        $product_cats_arr = $this->products->get_product_categories();    
        return $product_cats_arr;
    }

    /***** */

    Public function get_website_platforms() {
        $website_platforms_arr = $this->reviews->get_website_platforms();    
        return $website_platforms_arr;
    }

    public function review_exists($review_id) {
        return $this->reviews->review_exists($review_id);
    }    

    Public function get_review_by_id($review_id) {
        $review_arr = $this->reviews->get_review_by_id($review_id);    
        return $review_arr;
    }

    Public function get_reviews_by_user($user_id) {
        $reviews_arr = $this->reviews->get_reviews_by_user($user_id);    
        return $reviews_arr;
    }

    Public function get_reviews_by_influencer($influencer_id) {
        $reviews_arr = $this->reviews->get_reviews_by_influencer($influencer_id);    
        return $reviews_arr;
    }

    public function add_review ($user_id, $influencer_id, $product_id, $website_platform_id, $review_url, $notes) {
        if ($this->reviews->add_review ($user_id, $influencer_id, $product_id, $website_platform_id, $review_url, $notes)) {
            return true;
        } else {
            return false;
        }        
    }

    public function edit_review($user_id, $website_platform_id,$review_url,$review_id, $notes) {
        if ($this->reviews->edit_review($user_id, $website_platform_id,$review_url,$review_id, $notes)) {
            return true;
        } else {
            return false;
        }        
    }

    public function influencer_exists($influencer_id) {
        return $this->influencers->influencer_exists($influencer_id);
    }

    public function get_influencers() {
        $influencers_arr = $this->influencers->get_influencers();    
        return $influencers_arr;
    }    

    public function get_influencer_by_id($influencer_id) {
        $influencers_arr = $this->influencers->get_influencer_by_id($influencer_id);    
        return $influencers_arr;
    } 

    public function resize_image($source, $destination, $maxDimW, $maxDimH) {
        list($width, $height, $type, $attr) = getimagesize($source);
        if ($width > $maxDimW || $height > $maxDimH) {
            $target_filename = $source;
            $fn = $source;
            $size = getimagesize($fn);
            $ratio = $size[0] / $size[1]; // width/height
            if ($ratio > 1) {
                $width = $maxDimW;
                $height = $maxDimH / $ratio;
            } else {
                $width = $maxDimW * $ratio;
                $height = $maxDimH;
            }

            $imageSize = $this->getImageSizeKeepAspectRatio($source, $maxDimW, $maxDimH);

            $width = $imageSize['width'];
            $height = $imageSize['height'];

            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor($width, $height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);

            imagejpeg($dst, $target_filename); // adjust format as needed

        }
    }

    public function getImageSizeKeepAspectRatio($imageUrl, $maxWidth, $maxHeight)
    {
        $imageDimensions = getimagesize($imageUrl);
        $imageWidth = $imageDimensions[0];
        $imageHeight = $imageDimensions[1];
        $imageSize['width'] = $imageWidth;
        $imageSize['height'] = $imageHeight;
        if ($imageWidth > $maxWidth || $imageHeight > $maxHeight) {
            if ($imageWidth > $imageHeight) {
                $imageSize['height'] = floor(($imageHeight / $imageWidth) * $maxWidth);
                $imageSize['width'] = $maxWidth;
            } else {
                $imageSize['width'] = floor(($imageWidth / $imageHeight) * $maxHeight);
                $imageSize['height'] = $maxHeight;
            }
        }
        return $imageSize;
    }
    
}