<?php 

class Contactusmodel
{

    public function __construct() {
        
    }
    
    public function send_contact_email($post_data) {
        $email_message  =   '<strong>Sender name: '.$post_data['name'].'</strong><br />';
        $email_message .=   '<strong>Sender email: '.$post_data['email'].'</strong><br />';
        $email_message .=   '<strong>Sender message: </strong><br /><br />'.$post_data['message'].'<br />';
        $email_subject  =   'Influlaunch: Message from contact form!';
        $mailer = new Sysmail();
        if ($mailer->send_email_to_admin($post_data['email'], $post_data['name'], $email_subject, $email_message)) {
            return true;
        } else {
            return false;
        }
    }
    
}
